# Usage

## Repo Structure  
1. `backend` -- contains `R` code to perform differential expression analysis and
    supporting functions  
   - `DEAnalysis.r` -- script to generate the shiny app object  
   - `DETools.r` -- supporting functions used in `DEAnalysis.r`  
   - `exampleVars.csv` -- an example variable file  
   - `Shiny.Rproj` -- not useful, ignored  
2. `frontend` -- contains the `shiny` app  
   - `app.R` -- the `shiny` app  
   - `R` -- a folder containing `R` code for supporting  functions and modules that the app uses  
     - `global.r`
     - `mod_pseudoClip.r` -- a  clipboard module  
     - `mod_saveImagePDF.r` -- image saving module  
     - `mod_saveSelectedData.r` -- data (table) saving module  
     - `shiny.io.deployment.r` -- script for quickly getting an app onto shiny.io  
   - `www` -- a directory for CSS stuff  

## Getting Started 

1. Browsing the app with pre-canned data  
   1. Copy the `precanned.rds` file from `backend` to the `frontend` directory
   2. Start the app in R using `shiny::runApp("../frontend")`. Where `..` assumes 
     you started in `backend`, if not, simply substitue `..` for the full path to `frontend`.  

2. Running `DEAnalysis.r` with pre-canned data  # TODO: add pre-canned countMatrix.tsv
   - `DEanalysis.r` will run start to finish and generate two files starting with 
      `paste("edgeR_DEGs", Sys.Date(), sep = "_")`. 
     1. An `.xlsx` file contains 4 tabs with the full analysis results. 
     2. An `.rds` file containing the list object that the app uses. 
   - Copy the newly generated `rds` file to `frontend`. Run the app as before
   `shiny::runApp("../frontend")`.  

3. Running `DEAnalysis.r` with your data
   - What you will need.
     1. A count matrix -- we use STAR and the gencode GTF annotation
     2. An annotation data.frame -- This **MUST** contain at least 2 columns:
        the first being `gene_id` which matches the rownames of your count matrix,
        and the second being `gene_name` which contains human readable names. 
        * In an alternate universe we will fix this problem before it becomes a 
          dependency.
     3. A `.gmt` file from MsigDb where `gene_name` from the annotation file matches
        the gene names in the gmt file.
     4. (OPTIONAL): A network file with 3 columns `source`, `target`, and `species`
        `source` and `target` must match `gene_name` values. 
   - Assuming the above are met, things should be just peachy.
   
## Common Issues

1. There are a LOT of libraries required and since packaging shiny apps is kind of
   a nightmare, I've included a function for checking all the pre-req packages.
   After sourcing `DETools.r`, run `listPrereqs()` just to be sure.

## App Documentation

https://chinenovy.gitlab.io/drama_manual/





