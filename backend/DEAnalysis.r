# pull in libraries
suppressPackageStartupMessages({
    library(dplyr)
    library(magrittr) 
    library(edgeR)
    # library(data.table)
    library(writexl)
    library(qusage)
    library(parallel)
    library(sva)
    library(msigdbr)
})

source('DETools.r')

###---------------------------------------------------------------------------------------
# Load and Clean data
###---------------------------------------------------------------------------------------
# First read in the counts from 14584
x <- 
    read.delim(file = "countMatrix.tsv", row.names = 1, check.names = FALSE) %>% 
    dplyr::mutate(gene_name = NULL) %>% 
    # removing all genes that do not express at all anywhere
    .[rowSums(.) != 0,] %>%  
    filter(!grepl("PAR", rownames(.)))%>%
    set_colnames(., strsplit2(colnames(.), split = "\\.")[, 1]) %>%
    set_rownames(., strsplit2(rownames(.), split = "\\.")[,1])

# pull in the annotation
anno <- readRDS("anno_hg38.rds") %>% .[!duplicated(.$gene_name),]
# if you don't have a pre-built annotation, create one from a gencode gtf
# anno <- gencodeAnnoGenerate("homo_sapiens_hg38_gencode.gtf") %>% .[!duplicated(.$gene_name),]
look(anno) # look at head and tail from DETools.r

# THESE MUST PASS OR THINGS WILL BREAK
# MUST BE gene_id (sorry)
sum(rownames(x) %in% anno$gene_id) > 0.5*nrow(x) # at least 50% of genes annotated

# matching annotation and data table 
x <- x[rownames(x) %in% anno$gene_id,]
anno <- anno[match(rownames(x), anno$gene_id),]
identical(rownames(x), anno$gene_id)

# Make a table of variables (phenodat, targets, whatever you like to call them)
# Start with the names of x (we'll fix them later if they are ugly now)
### NOT RUN (run this if you want to make your own experimental variable table)
# if you have HSS AWS sample sheet:
# awssamples <- read.delim("SampleSheet.tsv")[,-1] |> 
#     select(-readtype, -lane) |> 
#     distinct()
# colnames(x) %>% data.frame(sample_id = .) %>% 
#     left_join(awssamples, by = join_by(sample_id)) |> 
#     write.csv(x = _, file = "experimentVars.csv", row.names = F)
# if not:
# colnames(x) %>% data.frame() %>% write.csv(x = , file = "experimentVars.csv", row.names = F)
### END NOT RUN

# After editing the csv file above, load it back into R
# An alternative to this is just build the data.frame in R
experimentVars <- read.csv(file = "experimentVars.csv", stringsAsFactors = TRUE, row.names = 1)

look(experimentVars)

identical(colnames(x), rownames(experimentVars))

###---------------------------------------------------------------------------------------
# Check for Sex
###---------------------------------------------------------------------------------------

# A lot of times we don't know the sex so we find it from XIST and DDX3Y expression
### for Humans: XIST - ENSG00000229807 ; DDX3Y - ENSG00000067048
### for Mice: Xist - ENSMUSG00000086503 ; Ddx3y - ENSMUSG00000069045
### for Rats: Xist -  ; Ddx3y - ENSRNOG00000002501

isFemale <-
    x[grep("ENSG00000229807|ENSG00000067048", rownames(x)),] %>% t() %>%
    apply(., 1, function(x) { x[1] > x[2] })

# optionally assign this to a variable
# sex <- sapply(isFemale, ifelse, "Female", "Male") %>% factor()

# check counts on the Y chromosome and verify consistency with sex assignment
# colSums(x[anno$seqnames == "chrY",]) %>%
#     data.frame(Ycounts = .) |>
#     cbind(t(x[grep("ENSG00000229807|ENSG00000067048", rownames(x)),])) |>
#     set_colnames(c("chrY", "XIST", "DDX3Y")) |>
#     cbind(sex)

# add it to experimentVars
# experimentVars$sex <- sex



###-----------------------------------------------------------------------------
# Create Experiment Information Object
###-----------------------------------------------------------------------------

#######
# N.B.> This function has new parameters! 
#
# sampleNames: supply a vector of names, rather than letting the function generate them
# excludeVarInNames: same as before, but no longer accepts NULL (just omit to not use)
# dontRename: skip renaming all together !!!! DEFAULT = TRUE !!!!
#
# with dontRename = TRUE, both sampleNames and excludeVarInNames are ignored
#######
experiment <- buildExperiment(countMatrix = x, 
                              experimentName = "Human.Drug1.Drug2", # use a meaningful name
                              variableTable = experimentVars, # table of variables
                              dontRename = TRUE) # change to false if you want new names
# confirm new names are correct
data.frame(experiment$newNames)
# rename your original data (old names are stored in the experiment object)
colnames(x) <- experiment$newNames

### --------------------------------------------------------------------------------------
# Perform Differential Expression Analysis
### --------------------------------------------------------------------------------------

# create a factor for group info
group <- 
    experiment$experimentVariables$group %>% 
    factor(., levels = c("Rest", 
                         "Drug1_3hr", "Drug2_3hr","Drug1_Drug2_3hr", 
                         "Drug1_6hr", "Drug2_6hr","Drug1_Drug2_6hr"))

# create factor for donor 
donor <- experiment$experimentVariables$donor

# design no intercept model
design <- 
    model.matrix(~ 0 + group + donor) %>% 
    set_colnames(., gsub("group|donor", "", colnames(.))) %>%
    set_rownames(., colnames(x))

# check if the design will work before proceeding.
is.fullrank(design)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# # # 
# N.B. CONTRASTS MUST BE NAMED! 
# # # 
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
c_matrix <- makeContrasts(
    # stardard named contrasts
    Drug1_vs_Rest = Drug1_3hr - Rest,
    Drug2_vs_Rest = Drug2_3hr - Rest,
    Drug2_vs_Drug1 = Drug2_3hr - Drug1_3hr,
    # ANOVA-like contrasts (Any Difference drug1 vs drug2)
    ANOVA = (Drug2_6hr + Drug2_3hr)/2 - (Drug1_6hr + Drug1_3hr)/2,
    # Interaction Term
    INTERACTION = (Drug2_6hr - Drug2_3hr) - (Drug1_6hr - Drug1_3hr),
    # "Synergy" at 3hrs
    SYNERGY = (Drug1_Drug2_3hr - Rest) - ((Drug1_3hr - Rest) + (Drug2_3hr - Rest)),
    levels = design)
# TODO: add additional examples of "synergy" contrasts see:
# https://github.com/nadschro/synergy-analysis?tab=readme-ov-file

# using the date is convenient when you need to reanalyze later
file.prefix <- paste("edgeR_DEGs", Sys.Date(), sep = "_")

# new alternative options
# # # # # # 
# # OPTION 1 :: local file # # #
# # # # # # 
gmt.path <- "c2.cp.v2023.1.Hs.symbols.gmt"

# # # # # # 
# # OPTION 2 :: msigdbr default # # #
# # This option will call msigdbr with species = "Homo sapiens", category = "C2", subcategory = "CP"
# # # # # # 
# gmt.path <- "msigdbr"

# # # # # # 
# # OPTION 3 :: custom msigdbr # # #
# # Passing a pre-made pathways list now works just fine
# # # # # # 
# gmt.path <- msigdbr(species = "Mus musculus", category = "C2", subcategory = "CGP")
# gmt.path <- split(gmt.path$gene_symbol, f = gmt.path$gs_name)
# # # # N.B. make sure to capitalize non-human gene names
# gmt.path <- sapply(gmt.path, toupper)
# # # N.B the gmt.describe parameter is provided so that additional tracking information
# # #     such as species/category/subcategory can be provided

DEresults <- getDE(        
    # first the data 
    counts          = x,           # a count matrix
    anno            = anno,        # the annotation data.frame
    experiment      = experiment,  # the buildExperiment output 
    design          = design,      # the model.matrix output
    contrast        = c_matrix,    # the makeContrasts output 
    group           = group,       # the grouping variable
    nuisanceVars    = "donor",     # a character string or character vector of variables to remove
    nuisanceCoVars  = NULL,        # same as nuisanceVars, but continuous variables
    # filtering parameters
    cutoff          = 3,           # minimum CPMs per group
    minGroupCutoff  = 1,           # minimum number of groups  
    minPathwayGenes = 10,          # minimum number of genes in a pathway
    maxPathwayGenes = 1000,        # maximum number of genes in a pathway
    # modeling parameters
    prior           = 2,           # updated prior  
    minLogFC        = 0,           # setting this non-0 will use glmTreat
    useFitMethod    = "qlf",       # glmFit or glmQLFit 
    useStatMethod   = "qlf",       # see glmLRT or glmQLFTest
    # output parameters
    out.prefix      = file.prefix, # saved file name
    countNormMethod = "cpm",       # cpms, tpms, rpkms...
    log.t           = TRUE,        # log transform CPMs? 
    save.xlsx       = TRUE,        # save an xlsx of the analysis
    doQuSage        = TRUE,        # run pathway or GO analysis with QuSage?
    QuSage.Boots    = 2^15,        # 2^15 is the default
    gmt.source      = gmt.path,    # gmt file OR list of pathways or Gene Ontologies
    filter.gmt      = NULL,        # list(gs_subcat = "CP:REACTOME"),
    gmt.describe    = "",          # optional description of gmt 
    qcPlots         = TRUE
)

saveRDS(object = DEresults, file = paste0(file.prefix, ".rds"))


### NOT RUN 
# To check the results interactively you need to move the rds to the "frontend" directory
# (any directory containing, app.R, global.r, www, and your resutls as .rds will work)
# system('cp edgeR_DEGs_*.rds ../frontend')
# system('cp 'RegNetwork_hu_mm_combined_no_mirna.csv ../frontend')
# shiny::runApp("../frontend")
### END NOT RUN



