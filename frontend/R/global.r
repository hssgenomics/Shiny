# global.r for DRaMA

#' colors_103 a color palette for large datasets with many categorical variables
#'
#' @description A vector of hexidecimal colors which are distinct enough to be
#' used for large sets of variables at random without becoming indistinguishable. 
#'
#' @export
colors_103 <- c("#E64B35", "#4DBBD5", "#00A087", "#3C5488", "#F39B7F", "#8491B4", 
                "#91D1C2", "#DC0000", "#7E6148", "#B09C85", "#1CE6FF", "#FF34FF", 
                "#008941", "#006FA6", "#A30059", "#7A4900", # "#FFDBE5",
                "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87", 
                "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80", "#61615A", 
                "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", 
                "#D16100", "#000035", "#7B4F4B", "#A1C299", "#300018", # "#DDEFFF", 
                "#0AA6D8", "#013349", "#00846F", "#372101", "#FFB500", # "#C2FFED", 
                "#A079BF", "#CC0744", "#C0B9B2", "#001E59", "#00489C", # "#C2FF99", 
                "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", 
                "#788D66", "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", 
                "#456648", "#0086ED", "#886F4C", "#34362D", "#B4A8BD", "#00A6AA", 
                "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81", "#575329", 
                "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", 
                "#1E6E00", "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", 
                "#772600", "#D790FF", "#9B9700", "#549E79", "#201625", "#FF4A46", 
                "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329", "#5B4534", 
                "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C")

###################################################################
#                      Helper Functions                           #
###################################################################

#' Converts a character string to a valid regex search pattern
#' 
#' @description strToRegex captures punctuation and white spaces and ensures
#' they are properly formatted for regex searches
#' 
#' @param x A string which should be translated to regex expression to search for said string
#' 
#' @returns A character string as a valid regex expression
#' 
#' @examples 
#' # strToRegex("big.green monster")
#' 
#' @export
strToRegex <- function(x){
    origStr <- x
    xChars <- unlist(strsplit(x, ""))
    subCharIdx <- grep("[[:punct:]]", xChars)
    wsCharIdx <- grep("\\s", xChars)
    subChars <- xChars[subCharIdx]
    regexChars <- paste0("\\", subChars)
    xChars[subCharIdx] <- regexChars
    xChars[wsCharIdx] <- "\\s"
    regexStr <- paste(xChars, collapse = "")
    return(regexStr)
}


#--------------
# FUNCTION
#    is.empty
# COMMENT
#    tests if a list is just a bunch of empty elements. e.g. [[1]] NULL [[2]] NULL etc
# REQUIRES
#    
# VARIABLES
#    x is a list to check the length of filled elements
# RETURNS
#    Logical: TRUE/FALSE

#' Check if a list is empty 
#' 
#' @description
#' A helper function for checking if a list contains any elements. Useful when 
#' a function might return a 0-length object e.g. integer(0)
#' 
#' @param x A list
#' 
#' @returns description
#' 
#' @export
is.empty <- function(x){ 
    stopifnot(is.list(x))
    ifelse(length(Filter(length, x)) < 1, TRUE, FALSE)
}   

ifelsenull <- function(x){
    stopifnot(is.logical(x) & length(x) == 1)
    if(x) TRUE else NULL
}

#--------------
# FUNCTION
#    hline and vline
# COMMENT
#    these functions support the volcano plotting fucntion
#    used for drawing horizontal or vertical lines on a plotly plot
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
# helper function to draw vertical line
vline <- function(x = 0, color = "red") {
    list(type = "line", 
         x0 = x, x1 = x,
         y0 = 0, y1 = 1, 
         yref = "paper", 
         line = list(color = color, dash = "dot")) 
}
# helper function to draw horizontal line
hline <- function(y = 0, color = "blue") {
    list(type = "line", 
         x0 = 0, x1 = 1, 
         y0 = y, y1 = y,
         xref = "paper", 
         line = list(color = color, dash = "dash")) 
}

###################################################################
#                        Main Functions                           #
###################################################################

#--------------
# FUNCTION
#    generateCorrPlot
# COMMENT
#    
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
generateCorrPlot <- function(corrData, corrLabs, corrMethod, corrColors, corrDend, corrCut, corrMain){
    if(corrCut == 0){ corrCut <- NA }
    corrPlotDat <- 
        cor(corrData, method = corrMethod, use = "pairwise.complete.obs") %>% 
        heatmapr(labCol = corrLabs,
                 labRow = corrLabs, 
                 k_row = corrCut, 
                 k_col = corrCut,
                 dendrogram = corrDend) 
    corrPlot <- 
        heatmaply(corrPlotDat, 
                  colors = viridis(n = 256, option = corrColors),
                  plot_method = "plotly", 
                  key.title = "Correlation\nCoefficient",
                  main = corrMain) %>% 
        colorbar(tickfont = list(size = 12), thickness = 20, len = 0.4, which = 1)
    return(list(corrPlotDat, corrPlot))
}

#--------------
# FUNCTION
#    generateCorrPlot
# COMMENT
#    
# REQUIRES
#    
# VARIABLES
#    
# RETURNS
#    
generateCustomHeat <- function(heatDat, heatCols, kTitle, heatTitle, covCols, dTick, 
                               k_row_cut, heatCovariatesVar, colLen, whichDendrogram, ...){
    
    if(whichDendrogram %in% c("column", "none") || k_row_cut == 0){
        k_row_cut <- NA
    }
    # put in set seed here
    set.seed(2)
    custHeatPlotDat <- heatmapr2(x = heatDat, 
                                 # height = ifelse(nrow(heatDat) * 15 < 500, 500,
                                 #                 nrow(heatDat) * 15),
                                 col_side_colors = heatCovariatesVar,
                                 k_row = k_row_cut,
                                 dendrogram = whichDendrogram,
                                 ...)
    set.seed(2)
    custHeatPlot <- heatmaply(x = custHeatPlotDat, 
                              height = ifelse(nrow(heatDat) * 15 < 500, 500,
                                              nrow(heatDat) * 15),
                              colors = heatCols,
                              plot_method = "plotly",
                              key.title = kTitle,
                              main = heatTitle, 
                              col_side_palette = covCols
                              ) %>% 
        layout(margin = list(l = 100, b = 100))
    if(is.null(heatCovariatesVar)){
        custHeatPlot <- colorbar(custHeatPlot, 
                                 tickfont = list(size = 12), 
                                 lenmode = 'pixels', 
                                 len = 200,
                                 tickmode = "linear", 
                                 tick0 = floor(min(heatDat)),
                                 dtick = dTick, 
                                 which = 1)
    } else {
        custHeatPlot <- 
            colorbar(custHeatPlot, 
                     tickfont = list(size = 10), 
                     lenmode = 'pixels', 
                     len = 200, 
                     y = 1, 
                     which = 1) %>%
            colorbar(custHeatPlot, 
                     tickfont = list(size = 10), 
                     lenmode = 'pixels', 
                     len = 120, 
                     y = -0.2,
                     tickmode = "linear", 
                     tick0 = floor(min(heatDat)), 
                     dtick = dTick, which = 2)
    }
    return(list(custHeatPlotDat, custHeatPlot))
}

#--------------
# FUNCTION
#    sortTransformedCounts
# COMMENT
#    replaces varianceSortedTransformedCounts since it can now be sorted by 
#    bcv or pairwise logfc
# REQUIRES
#    edgeR
# VARIABLES
#    edgeROut -- the object output by the getDE() function
#    sortMethod -- method for ordering data either "variance" or "logFC"
# RETURNS
#    transformed counts re-ordered by logFC or variance
sortTransformedCounts <- function(edgeROut, 
                                  sortMethod = c("variance", "logFC")){
    if(sortMethod == "logFC"){
        # this isn't actual logFC, just an estimate
        normDat <- edgeR::cpmByGroup(edgeROut$dgeList) 
        # compute pairwise "logFC" between groups
        hold <- lapply(2:ncol(normDat), function(i) {
            log((normDat[, i] / normDat[, i - 1]) + 0.2)
        }) 
        sx <- matrix(unlist(hold), ncol = length(hold), byrow = F) %>% 
            apply(X = ., MARGIN = 1, FUN = function(x){ mean(x^2) }) %>% 
            sort.list(x = ., decreasing = T, method = "radix")
        x <- edgeROut$transformedCounts[sx, ]
    }
    else if(sortMethod == "variance"){
        x <- sort.list(edgeROut$dgeList$tagwise.dispersion, decreasing = T, method = "radix") %>% 
            edgeROut$transformedCounts[., ]
    }
    return(as.data.frame(x))
}


#--------------
# FUNCTION
#    geneTableRender
# COMMENT
#    generates a fancy table for gene expression data
# REQUIRES
#    DT, dplyr
# VARIABLES
#    table -- a data.frame of gene expression data
#    title -- a string title
# RETURNS
#    hopefully a datatable...
geneTableRender <- function(table, title = ""){
    if(!is.data.frame(table)){
        stop(paste0("Data frame is expected but ", class(table)," found instead"))
    }
    if("gene_id" %in% colnames(table)){
        table$gene_id <- paste0('<a href="https://ncbi.nlm.nih.gov/gene/?term=', table$gene_id, 
                                '" target="_blank">', table$gene_id, '</a>')
        
    } else { 
        table$gene_id <- paste0('<a href="https://ncbi.nlm.nih.gov/gene/?term=', rownames(table), 
                                '" target="_blank">', rownames(table), '</a>') 
        table <- dplyr::select(table, gene_id, everything())
        
    }
  
    dt <- DT::renderDT({
        DT::datatable(table,
                      class = "compact stripe",
                      escape = FALSE,
                      caption = title,
                      filter = 'top',
                      rownames = F,
                      extensions = c('ColReorder','FixedColumns'), 
                      options = list(scrollX = T,
                                     pageLength = 5,
                                     colReorder = T,
                                     fixedColumns = list(leftColumns = 1))) %>%
        DT::formatSignif(columns = which(sapply(table, is.numeric)), digits = 4)
    }) 
    return(dt)
}    

#--------------
# FUNCTION
#    gtGene2
# COMMENT
#    
# REQUIRES
#    dplyr, reshape2, ggplot2, scales
# VARIABLES
#    edgeRObject - the output from the getDE() function
#    glist - a vector of gene names
#    gtable - a dataframe with gene expression in columns. 
#             In this context it takes data from out_list$rawCount or out_list$transformedCount
#    ytitle - Y-axis title - is not currently in use
#    ptitle - plot title
#    byGroup - 
#    colorPal - a color palette
#    pltcols - number of bargraph in a row
# RETURNS
#    a ggplot
gtGene2 <- function(edgeRObject = myDEOutput,
                    glist = NULL, 
                    gExpression = "CPM",
                    # gtable = NULL, 
                    byGroup = "All", 
                    scaleYes = T,
                    ptitle = "", 
                    # ytitle = "", 
                    colorPal = npgColors,
                    plotcols = 5) {
    
    if(scaleYes){ scale = "free" } else { scale = "free_x" }
    
    ### generate gtable within the function
    switch(gExpression,
           rawCounts = {
               gtable <- 
                   glob2rx(glist) %>% 
                   paste(., collapse = "|") %>% 
                   grep(., toupper(edgeRObject$rawCounts$gene_name)) %>% 
                   edgeRObject$rawCounts[.,]
               ytitle <- "Raw Counts"
           },
           CPM = {
               gtable <- 
                   glob2rx(glist) %>% 
                   paste(., collapse = "|") %>% 
                   grep(., toupper(edgeRObject$transformedCounts$gene_name)) %>% 
                   edgeRObject$transformedCounts[.,]
               ytitle <- "Log2 CPMs"
           }
    )
    
    gtable$gene_name <-  toupper(gtable$gene_name)
    lookup <- data.frame(sample = rownames(edgeRObject$dgeList$samples), 
                         group = edgeRObject$dgeList$samples$group)
    mat_melt <- 
        tidyr::pivot_longer(gtable, where(is.numeric), names_to = "sample_id") %>% 
        mutate(Group = factor(x = lookup$group[match(sample_id, lookup$sample)], 
                              levels = levels(edgeRObject$dgeList$samples$group)),
               gene_name = as.factor(gene_name))

    if("All" %in% byGroup){ 
        byGroup =  levels(mat_melt$Group) 
    } else {  
        mat_melt <- droplevels(mat_melt[mat_melt$Group %in% byGroup,])
    } 
    
    mat_melt_s <- 
        group_by(mat_melt, gene_name, Group) %>% 
        summarize(., sd = sd(value, na.rm = T), value = mean(value))
    
    #length of the longest X-label
    labLength <- round(max(sapply(unique(as.character(mat_melt$Group)),nchar))*0.45) 
    rowScaling <-  ceiling(dim(gtable)[1]/plotcols)    # number of rows given plotcols column
    # number of groups taken directly from a table that will be used in ggplot
    #gGroups <- length(unique(mat_melt_s$Group))        
    geneNumber <- length(unique(mat_melt_s$gene_name)) # number of genes to be  ploted
    
    #ignore warning about "unknown aestics" - it is just undocumented
    geneBar <- 
        ggplot(data = mat_melt, aes(x = factor(Group,levels = byGroup), y = value)) + 
        geom_point(aes(text = paste("Sample ID:", mat_melt$sample_id)), size = 1.5) + # , alpha = 0.7
        geom_bar(data = mat_melt_s, 
                 aes(fill = factor(Group,levels = byGroup)), color = "black", stat = "identity",
                 width = 0.8, alpha = 0.8, position = position_dodge(width = 0.3)) +
        geom_errorbar(data = mat_melt_s, 
                      aes(ymin = value, ymax = value + sd), width = 0.05, 
                      position = position_dodge(width = 0.3)) +
        facet_wrap(. ~gene_name, scales = "free", ncol = plotcols, drop = T) +
        labs(title = ptitle, x = "", y = ytitle) + 
        scale_fill_manual(values = colorPal) +
        scale_y_continuous(breaks = scales::pretty_breaks(n = 6)) + 
        theme_classic() +
        theme(panel.spacing.y = unit(labLength, "char"),
              strip.text = element_text(size = 10, face = "bold"),
              legend.position = "none", 
              plot.margin = margin(l = 35),
              axis.text.x = element_text(angle = 45, hjust = 1, vjust = 0.5, size = 10))  
    
    geneBar <- ggplotly(geneBar, height = rowScaling*350, tooltip = c("text","y")) %>% 
        config(displayModeBar = F)  
    
    #The section below from here to return(geneBar) involves manual editing or geneBar 
    #plotly object to correct some issues with layout calculation by plotlyR and ggplot. 
    #As such this section relies on the consistency of plotly and ggplotly outputs. 
    #If plotly developers change the structure of the object or correct output the issues 
    #I am dealing with this section may have to be adjusted/removed 
    
    numberOfRows <- ceiling(geneNumber/plotcols)
    
    #relative Y0 coordinates for each individual plot taken directly from a generated plotly object
    Y0s <- sapply(seq.int(from = 1, to = length(geneBar$x$layout$shapes), by = 2), function(y) { 
        geneBar$x$layout$shapes[[y]]$y0 
    }) %>% unique() 
    #relative Y1 coordinates for each individual plot taken directly from generated plotly object
    Y1s <- sapply(seq_along(geneBar$x$layout$shapes), function(y) {
        if(y%%2 == 1){
            geneBar$x$layout$shapes[[y]]$y1
        } else { 
            geneBar$x$layout$shapes[[y-1]]$y1 
        }
    }) %>% unique()
    
    #for some reason first and last rows of a plotly object are taller than other rows, 
    #same for columns - the difference is calculated here
    if(numberOfRows == 2){
        extraSpaceY = (0.5-(Y1s[1]-Y0s[1]))/1.5
    } else { 
        extraSpaceY <- ((Y1s[1]-Y0s[1]) - (Y1s[2]-Y0s[2]))/1.5
    }
    
    #extraSpaceX <- (X1s[1]-X0s[1]) - (X1s[2]-X0s[2])
    #I can calculate it (see above) but it is defined by ggplot and as panel.spacing.x 
    # has never being changed it is same
    extraSpaceX <- 0.0164378008332743 
    #for all plotcol values
    
    #Recalculate all X Y relative positions to deal with an error in margin calculation 
    #for the first/last rows/column it also provide a more controled layout for barplots
    newX0 <- seq.int(from = 0, to = 1, by = 1 / plotcols) + extraSpaceX  
    newX0 <- rep(newX0[-length(newX0)], numberOfRows)
    newX1 <- seq.int(from = 0, to = 1, by = 1 / plotcols) - extraSpaceX
    newX1 <- rep(newX1[-1], numberOfRows)
    
    if(numberOfRows == 1){ 
        newY0 = rep(0, plotcols)
        newY1 = rep(1, plotcols)
    } else { 
        newY0 <- seq.int(from = 1, to = 0, by = -1 / numberOfRows) + extraSpaceY
        newY0 <- newY0[-1] 
        newY0 <- c(sapply(1:length(newY0), function(y) rep(newY0[y], plotcols)))
        newY1 <- seq.int(from = 1, to = 0, by = -1 / numberOfRows) - extraSpaceY
        newY1 <- newY1[-length(newY1)]
        newY1 <- c(sapply(1:length(newY1), function(y) rep(newY1[y], plotcols)))
    }
    
    #index for all all xaxix elements in geneBar$x$layout
    #yaxisIndex is xaxisIndex +1 - no need to calculate it separately
    xaxisIndex <- grep("xaxis",names(geneBar$x$layout))
    
    for(i in 1:length(xaxisIndex)){
        geneBar$x$layout[[xaxisIndex[i]]]$domain[1] <- newX0[i]
        geneBar$x$layout[[xaxisIndex[i]]]$domain[2] <- newX1[i]
        geneBar$x$layout[[xaxisIndex[i]+1]]$domain[1] <- newY0[i]
        geneBar$x$layout[[xaxisIndex[i]+1]]$domain[2] <- newY1[i]
    }

    #$Shapes basically - get rid of them.
    #This get rid of boxes around gene names
    #NB! use strip.background = element_blank() in geneBar theme definition makes boxes 
    #invisible but does not remove element descriptions
    #So the SIZE of output plotly object remains the same whether you see boxes or not. 
    #These two lines below actually makes plotly object MUCH smaller 
    boxIndex <- seq.int(from = 1, to = length(geneBar$x$layout$shapes), by = 1)
    geneBar$x$layout$shapes <- geneBar$x$layout$shapes[-boxIndex]
    
    #$Annotations - move gene names to proper X positions
    #First element in annotation contains info on Y axe label, the rest are gene labels
    #sanity check (length(geneBar$x$annotation)-1) == geneNumber
    for(i in seq.int(from = 2, to = length(geneBar$x$layout$annotations))){
        geneBar$x$layout$annotations[[i]]$x <- (newX0[i-1]+newX1[i-1])/2
        geneBar$x$layout$annotations[[i]]$y <- newY1[i-1]
    }
    
    return(geneBar)
}

#--------------
# FUNCTION
#    gtGene3
# COMMENT
#    
# REQUIRES
#    dplyr, reshape2, ggplot2, scales
# VARIABLES
#    edgeRObject - the output from the getDE() function
#    glist - a vector of gene names
#    gtable - a dataframe with gene expression in columns. In this context it takes data 
#             from out_list$rawCount or out_list$transformedCount
#    ytitle - Y-axis title - is not currently in use
#    ptitle - plot title
#    byGroup = "All" - a vector of group names. NB because in thiss app group names are 
#              passed from the selection box the function does not check whether
#              groups actually exist. For a standalone function this check needs to be added
#    scaleYes = T , if scaleYes=T the Y axis will be shared for all subplots           
#    colorPal - a color palette 
#    range - "nonnegative" | "normal" | "tozero" this is passed to rangemode attribute of yaxis (plotly) 
#            and used to improve subplot alignment
#    plotcols = 5 - pased to subplot(widths=) to shrink the width of an individual bargraph 
#               when the number of genes <= plotcols 
#    ytitle = "" goes into yaxix = list(title=) of a plotly layout
# RETURNS
#    a list of plotly objects (or a plotly plot - see comments)
# gtGene3 <- function(edgeRObject = myDEOutput,
#                     glist = NULL, 
#                     gtable = NULL, 
#                     byGroup = "All", 
#                     ptitle = "", 
#                     ylabel = "", 
#                     scaleYes = T,
#                     colorPal = npgColors,
#                     range = "nonnegative",
#                     plotcols=5){
# 
#     if(length(unique(edgeRObject$dgeList$samples$group)) > length(colorPal)){
#         reScaleColors <- colorPal
#     } else {
#         reScaleColors <- colorRampPalette(colorPal)(length(unique(edgeRObject$dgeList$samples$group)))
#     }
#     
#     gtable$gene_name <-  toupper(gtable$gene_name)
#     lookup <- data.frame(sample = rownames(edgeRObject$dgeList$samples), 
#                          group = edgeRObject$dgeList$samples$group)
#     
#     mat_melt <- 
#         gtable[gtable$gene_name %in% glist, ] %>% 
#         melt() %>% 
#         mutate(., Group = factor(lookup$group[match(variable, lookup$sample)], 
#                                  levels = levels(edgeRObject$dgeList$samples$group)),
#                gene_name = as.factor(gene_name))  
#     
#     if(!("All" %in% byGroup)){
#         mat_melt <-  droplevels(mat_melt[mat_melt$Group %in% byGroup,])
#     }
#     
#     mat_melt_s <- 
#         group_by(mat_melt, gene_name, Group) %>% 
#         summarize(., sd = sd(value, na.rm = T), value = mean(value))
#     
#     ngroups <- length(unique(mat_melt_s$Group))
#     #plotlyOne is a function to create a barplot for ONE gene over all groups
#     #it uses column names from mat_melt_s and data points from mat_melt
#     
#     if(length(unique(mat_melt_s$gene_name)) <= plotcols){  
#         plotrows=1                                 
#         colwidth=rep(1/plotcols,length(unique(mat_melt_s$gene_name)))
#         
#     } else {
#         plotrows <- mat_melt_s$gene_name %>% unique %>% length %>% divide_by(plotcols) %>% ceiling()
#         colwidth <- NULL
#     }
#     
#     plotlyOne <- function(t){   
#         
#                  plot_ly(data = t,
#                          x = ~Group,
#                          y = ~value,
#                          type = "bar",
#                          color = ~Group,
#                          colors = colorPal,
#                          #at 300dpi it is one inch tall and about 3.5 inches on 75 DPI monitor
#                          height = 300*plotrows, 
#                          #ngroups*300, #the assuption here is that we need about 
#                          #  300 pixels per bar includin spacing and some spacing for Y-axis 
#                          width = NULL, 
#                          marker = list(line = list(color = 'rgb(0,0,0)',
#                                                  width = 1)),
#                          showlegend = FALSE,                          
#                          error_y = ~list(array = sd,
#                                          symmetric = F,
#                                          color = '#000000',
#                                          thickness = 1)) %>% 
#                 add_trace(data = mat_melt[mat_melt$gene_name == t$gene_name,],
#                                  x = ~Group, 
#                                  y = ~value,
#                                  type = "scatter",
#                                  mode = "markers",
#                                  marker = list(size = 7),
#                                  color = I("black")) %>%
#                 add_annotations(text = ~gene_name,
#                                 x = 0.5,
#                                 y = 1.04,
#                                 yref = "paper",
#                                 xref = "paper",
#                                 xanchor = "middle",
#                                 yanchor = "top",
#                                 showarrow = FALSE,
#                                 font = list(size = 15)) %>% 
#             layout(
#                   xaxis = list(showgrid = FALSE,
#                                showline = TRUE,
#                                tickangle = 45),
#                   yaxis = list(showgrid = FALSE,
#                                showline = TRUE,
#                                visible = TRUE,
#                                rangemode = range,
#                                pad = 0,
#                                title = ylabel,
#                                ticks = "outside",
#                                showticklabels = TRUE))
#                  
#     }
#     # when there are few plots a plot takes up an entire screen.
#     # colwidth is used to decreases the width of idividual plot to 1/plotcols*100% of available screen width (17% by default)
#     # and is used by sublot(). For a large number of plots it is not needed
#     # NB the way the number of columns is determined by plotly::subplot() is screwed up and needs to be fixed at the sourse
#     
#     mat_melt_s %>% 
#        split(.$gene_name) %>% 
#        lapply(plotlyOne) -> plotlyOne_list
#     
#     if(length(plotlyOne_list) %/% plotcols != length(plotlyOne_list) / plotcols)
#           {
#            Ndummy <-  (length(plotlyOne_list) %/% plotcols+1)*plotcols - length(plotlyOne_list)
#            plotlyDummy <- plotly_empty(
#                
#                type = "bar",
#                #at 300dpi it is one inch tall and about 3.5 inches on 75 DPI monitor
#                height =300*plotrows, 
#                #the assuption here is that we need about 120 pixels per bar includin spacing and some spacing for Y-axis 
#                width = NULL, 
#                marker = list(line=list(color='rgb(0,0,0)',
#                                        width=1)),
#                showlegend = FALSE                          
#                )
#            
#            for (i in seq(length(plotlyOne_list)+1,length(plotlyOne_list)+Ndummy,by=1)){
#                
#                plotlyOne_list[[i]] <- plotlyDummy}
#         } else {}
#        
#       
#         subplot(plotlyOne_list, margin = c(0,0.07,0.1,0),nrows=plotrows,  
#                shareY=scaleYes,titleY = T,which_layout = c(1)) %>% 
#         layout(margin = list(t = 50),
#                xaxis = list(showgrid = FALSE,
#                             showline = TRUE,
#                             tickangle = 45),
#                yaxis = list(showgrid = FALSE,
#                             showline = TRUE,
#                             visible = TRUE,
#                             rangemode = range,
#                             pad=10,
#                             title=ylabel,
#                             ticks = "outside",
#                             showticklabels = TRUE))
# }


#--------------
# FUNCTION
#    customValueBox
# COMMENT
#    Clickable InfoBox
#    stolen from https://stackoverflow.com/questions/37169039/direct-link-to-tabitem-with-r-shiny-dashboard
# REQUIRES
#    shiny
# VARIABLES
#    title, 
#    tab = NULL, 
#    value = NULL, 
#    subtitle = "", 
#    color = "navy", 
#    width = 4, 
#    href = NULL, 
#    fill = FALSE
# RETURNS
#    an HTML object??? idk
customValueBox <- function(title, 
                           tab = NULL, 
                           value = NULL, 
                           subtitle = "", 
                           color = "navy", 
                           width = 4, 
                           href = NULL, 
                           target = NULL,
                           fill = FALSE) {
    #shinydashboard::validateColor(color)
    #shinydashboard::tagAssert(icon, type = "i")
    colorClass <- paste0("bg-", color)
    boxContent <- div(class = "value-box", 
                      class = if(fill){ colorClass }, 
                      onclick = if(!is.null(tab)) { 
                          paste0("$('.sidebar a')).filter(function() { return ($(this).attr('data-value') == ", 
                                 tab, 
                                 ")}).click()") 
                      },
                      #span(class = "value-box-icon", class = if (!fill) colorClass, icon),  
                      div(class = "value-box-content", 
                          style = "font-size: 20px; font-weight:bold",
                          span(class = "value-box-text", title), 
                          if(!is.null(value)){ 
                              span(class = "value-box-number", value) 
                          }, 
                          if(!is.null(subtitle)){ 
                              p(subtitle) 
                          }
                      )
    )
    if(!is.null(href)){ 
        # Check if target is specified, and set the target attribute if provided
        if (!is.null(target)) {
            boxContent <- a(href = href, boxContent, target = target)
        } else {
            boxContent <- a(href = href, boxContent)
        }
    }
    div(class = if(!is.null(width)){ paste0("col-sm-", width) }, boxContent)
}


#--------------
# FUNCTION
#    plotlyPathways
# COMMENT
#    plotly-ification of plotCIs from qusage ¯\_(ツ)_/¯
# REQUIRES
#    plotly
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathway.subset = subset pathways in canonical pathways gmt
#    order.by = one of c("logFC", "PValue", "FDR")
#    signif.select = select significance by either c("PValue", "FDR")
#    signif.thresh = what is the significance threshold? default = 0.05
#    signif.only = should only significant results be plotted. default = FALSE
#    PDF = boolean: controls opacity of plot for PDF download. default = FALSE
#    ... = additional parameters passed to primary plot_ly() call
# RETURNS
#    a plotly bubble plot of pathways.

plotlyPathways <- function(qusageObject, 
                           pathway.subset = "ALL",
                           #order.by = c("logFC","PValue"),
                           signif.select = c("PValue", "FDR"),
                           signif.thresh = 0.05,
                           signif.only = FALSE, 
                           PDF = FALSE, ...){
    #stopifnot(length(order.by) == 1, length(signif.select) == 1)
    if(PDF){
        PDF <- 1
    } else { PDF <- 0.66 }
    
    # add color
    qusageObject$PathwayTable$color <- "grey"  # Default color
    
    significant_entries <- qusageObject$PathwayTable[qusageObject$PathwayTable[[signif.select]] <= signif.thresh, ]
    
    if (nrow(significant_entries) > 0) {
        # Generate color bins and palette
        
        fdrPColorPal <- colorRampPalette(rev(brewer.pal(9, "Reds")))(nrow(significant_entries))

        # Rank the significant entries and assign colors
        ranked_colors <- fdrPColorPal[rank(significant_entries[[signif.select]])]
        
        qusageObject$PathwayTable[rownames(significant_entries), "color"] <- ranked_colors
        
    } else {fdrPColorPal = rep("grey", nrow(qusageObject$PathwayTable))}
    
    pathwayDat <- 
        qusageObject$PathwayTable %>%
        dplyr::filter(complete.cases(.)) %>% # remove pathways that didn't have enough genes for stats
        dplyr::arrange("PValue") %>% # reorder pathways by PValue
        tibble::rownames_to_column(., "Pathway") %>% # move rownames to first column 
        dplyr::mutate( 
            lowCI = abs(lowCI), # use absolute value for easier plotting
            highCI = abs(highCI), 
            signif = !!sym(signif.select) < signif.thresh) %>% # make new variable for user defined significance
        # you can pipe to pretty much anything if it's in brackets
        # if subset is selected filter, otherwise return .
        { if(pathway.subset != "ALL") .[grep(paste0("^", pathway.subset), .$Pathway),] else . } %>% 
        { if(signif.only == TRUE) filter(., signif == TRUE) else . } 
    
    pathwayDat$index <-  rank(pathwayDat[,"PValue"]) %>% as.integer()
    pathwayDat$size_scaled <- rescale(pathwayDat$size, to = c(10,150)) #I preserve scaled circle size too so I can use it outside the function
   
     # colBarPars <- list(thickness = 15,
     #                    len = 0.35,
     #                    title = signif.select,
     #                    cmax = signif.thresh + 0.1,
     #                    tickmode = "auto") 
     
    # # for consistent colouring we only color by p-value so the range is 0 to 1
    # fdrPColorBins <- seq(min(pathwayDat$PValue), max(pathwayDat$PValue), length = nrow(pathwayDat))
    # # generate a grey to red color pallette
    # fdrPColorPal <- brewer.pal(9, "Reds")%>% rev(.) %>% colorRampPalette(.)
    # # extend color pallette to the same size as colorBins
    # names(fdrPColorBins) <- fdrPColorPal(nrow(pathwayDat))
    # # plotlyColors
    # plotlyColors <- names(fdrPColorBins)[findInterval(pathwayDat[,"PValue"], fdrPColorBins, all.inside = T)]
   
     
     #View(pathwayDat %>% filter(signif == TRUE))
     
    
     pathwayPlot <- pathwayDat %>% 
         ggplot(aes(key = Pathway, customdata = Pathway)) +
         
         geom_point(data = pathwayDat %>% filter(signif == F), 
                    aes(x = index, y = logFC, size = size, 
                        text = paste("Pathway: ", Pathway, "\n", 
                                     "Size: ", size, ", logFC: ", round( logFC, 3),",",  
                                     paste0(signif.select, ": "), 
                                     formatC(!!sym(signif.select), format = "e", 1))
                        ), 
                    fill = "grey", color = "white", shape = 21, alpha = 1, stroke = 0.3)+
         
         geom_point(data = pathwayDat %>% filter(signif == TRUE) ,
                   aes(x = index, y = logFC, size = size, fill = !!sym(signif.select),
                       text = paste("Pathway: ", Pathway, "\n", 
                                    "Size: ", size, ", logFC: ", round( logFC, 3),",",  
                                    paste0(signif.select, ": "), 
                                    formatC(!!sym(signif.select), format = "e", 1))
                       ),
                  shape = 21, color = "white", alpha = 0.75, stroke = 0.3)+
        
         scale_y_continuous("Pathway LogFC", n.breaks = 10, expand = expansion(mult = 0.1))+
         scale_x_continuous("Pathway rank",expand = expansion(mult = 0.1)) +
         
         scale_fill_gradientn(colors = fdrPColorPal)+
         
         scale_size (name = "Size", range = c(4, 25)) +
         guides(size = "none")+
         geom_hline(yintercept = 0,linewidth = 0.2, color = "grey30")+
         
         theme_classic() + theme(axis.text.x = element_blank(), axis.ticks.x = element_blank(),
                             panel.grid.major.x = element_blank(), panel.grid.minor.x = element_blank(),
                             axis.line = element_blank()) 
    
    # pathwayPlot <- pathwayDat %>% 
    #         plot_ly( 
    #                 x = ~index,
    #                 y = ~logFC, 
    #                 key = ~Pathway,
    #                 customdata = ~Pathway,
    #                 hoverinfo = 'text',
    #                 alpha = 1,
    #                 text = paste("Pathway: ", .$Pathway, "\n", 
    #                              "Size: ", .$size, ", logFC: ", round(.$logFC, 3),",",  
    #                              paste0(signif.select, ": "), 
    #                              formatC(.[,signif.select], format = "e", 1)),
    #                 type = 'scatter', 
    #                 mode = 'markers',
    #                 
    #                 marker = list(symbol = 'circle', 
    #                               color = ~get(signif.select),#~I(orderedColors),#,
    #                               #colors = I(orderedColors) ,
    #                               colorscale = colorscale,
    #                               size = ~size_scaled, #rescale(.$size, to = c(10,75)),
    #                               sizemode = 'diameter',
    #                               line = list(width = 1, color = "#FFFFFF"),
    #                               colorbar = colBarPars,
    #                               showscale = TRUE
    #                               )
    #                 ) %>% 
    #                 layout(dragmode = "select",
    #                      xaxis = list(showticklabels = F,
    #                             showgrid = F,
    #                             zeroline = F,
    #                             title = "Pathway rank"),
    #                         yaxis = list(title = "Pathway LogFC")
    #                         
    #                         #title = list(text = "<b>QuSAGE Pathway Analysis</b>",
    #                         #       y = 0.9, x = 0.5),
    #                #margin = list( l = 50, r = 50, b = 10, t = 80,  pad = 4)#,
    #                #hoverlabel = list(font = list( color = "black" ))
    #                
    #                ) %>% 
    #         config(displayModeBar = TRUE,
    #                displaylogo = FALSE,
    #                modeBarButtonsToRemove = list("pan2d", "lasso2d", "zoomIn2d",
    #                                              "zoomOut2d", "autoScale2d", 
    #                                              "hoverCompareCartesian")
    #                ) %>% event_register('plotly_click')

     
     pathwayPlot <- ggplotly(pathwayPlot,
                             tooltip = "text", source = "qusage") %>% 
         config(displayModeBar = TRUE,
                displaylogo = FALSE,
                modeBarButtonsToRemove = list("pan2d", "lasso2d", "zoomIn2d",
                                              "zoomOut2d", "autoScale2d",
                                              "hoverCompareCartesian")) %>% 
                event_register('plotly_click')
     
         
    returnList <- list(pathwayPlot = pathwayPlot, pathwayDat = pathwayDat)
        
    return(returnList)
}



#--------------
# FUNCTION
#    filterPathwayDownloadTable
# COMMENT
#    this is a companion to plotlyPathways. It generates a downloadable table corresponding
#    to the plotlyPathways plot.
# REQUIRES
#    tibble, plyr
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathway.subset = subset pathways in canonical pathways gmt
#    order.by = one of c("logFC", "PValue", "FDR")
#    signif.select = select significance by either c("PValue", "FDR")
#    signif.thresh = what is the significance threshold? default = 0.05
#    signif.only = should only significant results be plotted. default = FALSE
# RETURNS
#    pathwayDataAndGenes -- a table of data used for the plotlyPathways plot which can now be downloaded
#
# NOTE! THis function is usefull as a stanalone extractor but curently we do not use it in the app code

# filterPathwayDownloadTable <- function(qusageObject, 
#                                        pathway.subset = "ALL",
#                                        order.by = c("logFC","PValue"),
#                                        signif.select = c("PValue", "FDR"),
#                                        signif.thresh = 0.05,
#                                        signif.only = FALSE, ...){
#     stopifnot(length(order.by) == 1, length(signif.select) == 1)
#     pathwayDat <- 
#         qusageObject$PathwayTable %>% # grab the pathway stats table
#         #.[complete.cases(qusageObject$PathwayTable),] %>% # remove pathways that didn't have enough genes for stats
#         #.[order(.[order.by]),] %>% # reorder pathways by `order.by` variable
#         tibble::rownames_to_column(., "Pathway") %>% # move rownames to column
#         dplyr::mutate(  
#                      lowCI = abs(lowCI), # use absolute value for easier plotting
#                      highCI = abs(highCI), 
#                      # make new variable for user defined significance
#                      signif = .[,signif.select, drop = T] < signif.thresh) %>% 
#         # you can pipe to pretty much anything if it's in brackets
#         # if subset is selected filter for that using grep, otherwise return .
#                      { if(pathway.subset != "ALL") .[grep(paste0("^", pathway.subset), .$Pathway),] else . } %>% 
#                      { if(signif.only == TRUE) filter(., signif == TRUE) else . } # same as above but with signif
#     # this is a fucking nightmare of a function
#     # get the indexes of genes in selected pathways
#     stopifnot(nrow(pathwayDat) > 0)
#     pathwayDataAndGenes <- 
#         qusageObject$PathwayIndexs[names(qusageObject$PathwayIndexs) %in% pathwayDat$Pathway] %>% 
#         # convert indexes to gene names and then to a comma separated string
#         lapply(., function(x){ toString(rownames(qusageObject$GeneTable[x,]))}) %>% 
#         unlist %>% 
#         data.frame %>% 
#         set_colnames(., "Genes") %>% 
#         cbind(pathwayDat, .) %>% 
#         mutate(., signif = NULL) %>% 
#         .[order(.[,order.by]),]
#     
#     pathwayDataAndGenes$index =  rank(pathwayDataAndGenes[,order.by])
#      
#     return(pathwayDataAndGenes)
# }

#--------------
# FUNCTION
#    plotlyPathwayGenes
# COMMENT
#    plotly-ification of plotCIsGenes from qusage ¯\_(ツ)_/¯
# REQUIRES
#    plotly
# VARIABLES
#    qusageObject = qusageObject generated by qusage2
#    pathwayName = name of pathway to plot
#    color.points = color for points in plot
# RETURNS
#    pathGenesPlot -- a plot of genes from a pathway. Companion to plotlyPathways 
plotlyPathwayGenes <- function(qusageObject,
                               pathwayName = NULL,
                               color.points = "#313695", ...){
    pathGenesPlot <- 
        qusageObject$PathwayIndexs[[pathwayName]] %>% 
        qusageObject$GeneTable[.,] %>% 
        mutate(., name = rownames(.)) %>%
        .[order(.$mean),] %>% 
        mutate(., tickpos = 1:nrow(.))
    
    # if zero is not in the data range, 
    maxRange <- range(rowSums(pathGenesPlot[,c("mean", "sd")]))
    minRange <- range(apply(pathGenesPlot[,c("mean", "sd")], 1, function(x){x[1] - x[2]}))
    currentRange <- c(min(minRange, maxRange), max(minRange, maxRange))
    if(!between(0, currentRange[1], currentRange[2])){
        plotRange <- "tozero"
    } else { plotRange <- "normal" }
    
    pathGenesPlot %<>% 
    plot_ly(data = ., 
            x = 1:nrow(.),
            y = ~mean,
            hoverinfo = 'text',
            text = paste("Gene Name: ", .$name, "\n",
                         "logFC: ", round(.$mean, 3), "\n", 
                         sep = ""),
            type = 'scatter', 
            mode = 'markers',
            marker = list(size = 10, 
                          color = color.points),
            error_y = list(type = "data",
                           array = ~sd,
                           color = "#252525"),
            ...) %>% 
    layout(title = pathwayName,
           xaxis = list(showticklabels = T,
                        tickmode = "array", 
                        tickvals = ~tickpos, 
                        ticktext = ~name,
                        showgrid = F,
                        zeroline = F),
           yaxis = list(title = "Gene LogFC", 
                        zeroline = T, 
                        rangemode = plotRange)) %>% 
    config(modeBarButtonsToRemove = list("pan2d", "lasso2d", "zoomIn2d", "zoomOut2d", 
                                         "autoScale2d", "hoverCompareCartesian"),
           displayModeBar = TRUE,
           displaylogo = FALSE)
    pathGenesPlot
}


#--------------
# FUNCTION
#    heatmapr2 is heatmaply with better checks for col_side_colors
# COMMENT
#    this should be added to the feature requests of talgalili/heatmaply
# REQUIRES
#    heatmaply, dendextend, seriation, assertthat
# VARIABLES
#    all the same as heatmapr
# RETURNS
#    a heatmapr object to be passed to heatmaply

heatmapr2 <- function (x, Rowv, Colv, distfun = dist, hclustfun = hclust, 
                       dist_method = NULL, hclust_method = NULL, distfun_row, hclustfun_row, 
                       distfun_col, hclustfun_col, dendrogram = c("both", "row", "column", "none"), 
                       reorderfun = function(d, w) reorder(d, w), k_row = 1, k_col = 1, symm = FALSE, 
                       revC, scale = c("none", "row", "column"), na.rm = TRUE, labRow = rownames(x), 
                       labCol = colnames(x), cexRow, cexCol, digits = 3L, cellnote = NULL, 
                       theme = NULL, colors = "RdYlBu", width = NULL, height = NULL, 
                       xaxis_height = 80, yaxis_width = 120, xaxis_font_size = NULL, 
                       yaxis_font_size = NULL, brush_color = "#0000FF", show_grid = TRUE, 
                       anim_duration = 500, row_side_colors = NULL, col_side_colors = NULL, 
                       seriate = c("OLO", "mean", "none", "GW"), point_size_mat = NULL, 
                       custom_hovertext = NULL, ...) {

    distfun <- match.fun(distfun)
    if (!is.null(dist_method)) {
        distfun_old <- distfun
        distfun <- function(x) {
            distfun_old(x, method = dist_method)
        }
    }
    if (!is.null(hclust_method)) {
        hclustfun_old <- hclustfun
        hclustfun <- function(x) {
            hclustfun_old(x, method = hclust_method)
        }
    }
    if (missing(distfun_row)) 
        distfun_row <- distfun
    if (missing(hclustfun_row)) 
        hclustfun_row <- hclustfun
    if (missing(distfun_col)) 
        distfun_col <- distfun
    if (missing(hclustfun_col)) 
        hclustfun_col <- hclustfun
    distfun_row <- match.fun(distfun_row)
    distfun_col <- match.fun(distfun_col)
    if (!is.matrix(x)) {
        x <- as.matrix(x)
    }
    if (!is.matrix(x)) 
        stop("x must be a matrix")
    seriate <- match.arg(seriate)
    nr <- nrow(x)
    nc <- ncol(x)
    rownames(x) <- labRow %||% as.character(1:nrow(x))
    colnames(x) <- labCol %||% as.character(1:ncol(x))
    rownames(x) <- heatmaply:::fix_not_all_unique(rownames(x))
    colnames(x) <- heatmaply:::fix_not_all_unique(colnames(x))
    if (!missing(cexRow)) {
        if (is.numeric(cexRow)) {
            xaxis_font_size <- cexRow * 14
        }
        else {
            warning("cexRow is not numeric. It is ignored")
        }
    }
    if (!missing(cexCol)) {
        if (is.numeric(cexCol)) {
            yaxis_font_size <- cexCol * 14
        }
        else {
            warning("cexCol is not numeric. It is ignored")
        }
    }
    scale <- match.arg(scale)
    if (scale == "row") {
        x <- sweep(x, 1, rowMeans(x, na.rm = na.rm))
        x <- sweep(x, 1, apply(x, 1, sd, na.rm = na.rm), "/")
    }
    else if (scale == "column") {
        x <- sweep(x, 2, colMeans(x, na.rm = na.rm))
        x <- sweep(x, 2, apply(x, 2, sd, na.rm = na.rm), "/")
    }
    dendrogram <- match.arg(dendrogram)
    if (missing(Rowv)) {
        Rowv <- dendrogram %in% c("both", "row")
    }
    if (missing(Colv)) {
        if (dendrogram %in% c("both", "column")) {
            Colv <- if (symm) 
                "Rowv"
            else TRUE
        }
        else {
            Colv <- FALSE
        }
    }
    if (isTRUE(Rowv)) {
        Rowv <- switch(seriate, 
                       mean = rowMeans(x, na.rm = na.rm), 
                       none = 1:nrow(x), 
                       OLO = {
                           dist_x <- distfun_row(x)
                           hc_x <- hclustfun_row(dist_x)
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- seriate_dendrogram(dend_x, dist_x, 
                                                         method = "OLO")
                           dend_x2
                       }, 
                       GW = {
                           dist_x <- distfun_row(x)
                           hc_x <- hclustfun_row(dist_x)
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- seriate_dendrogram(dend_x, dist_x, 
                                                         method = "GW")
                           dend_x2
                       })
    }
    if (is.numeric(Rowv)) {
        Rowv <- reorderfun(as.dendrogram(hclustfun_row(distfun_row(x))), 
                           Rowv)
        Rowv <- rev(Rowv)
    }
    if (is.hclust(Rowv)) 
        Rowv <- as.dendrogram(Rowv)
    if (is.dendrogram(Rowv)) {
        rowInd <- order.dendrogram(Rowv)
        if (nr != length(rowInd)) {
            stop("Row dendrogram is the wrong size")
        }
    }
    else {
        if (!is.null(Rowv) && !is.na(Rowv) && !identical(Rowv, 
                                                         FALSE)) {
            warning("Invalid value for Rowv, ignoring")
        }
        Rowv <- NULL
        rowInd <- 1:nr
    }
    Rowv <- rev(Rowv)
    rowInd <- rev(rowInd)
    if (identical(Colv, "Rowv")) {
        Colv <- Rowv
    }
    if (isTRUE(Colv)) {
        Colv <- switch(seriate, mean = colMeans(x, na.rm = na.rm), 
                       none = 1:ncol(x), OLO = {
                           dist_x <- distfun_col(t(x))
                           hc_x <- hclustfun_col(dist_x)
                           o <- seriate(dist_x, method = "OLO", control = list(hclust = hc_x))
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- rotate(dend_x, order = rev(labels(dist_x)[get_order(o)]))
                           dend_x2
                       }, GW = {
                           dist_x <- distfun_col(t(x))
                           hc_x <- hclustfun_col(dist_x)
                           o <- seriate(dist_x, method = "GW", control = list(hclust = hc_x))
                           dend_x <- as.dendrogram(hc_x)
                           dend_x2 <- rotate(dend_x, order = rev(labels(dist_x)[get_order(o)]))
                           dend_x2
                       })
    }
    if (is.numeric(Colv)) {
        Colv <- reorderfun(as.dendrogram(hclustfun_col(distfun_col(t(x)))), 
                           rev(Colv))
    }
    if (is.hclust(Colv)) 
        Colv <- as.dendrogram(Colv)
    if (is.dendrogram(Colv)) {
        Colv <- rev(Colv)
        colInd <- order.dendrogram(Colv)
        if (nc != length(colInd)) {
            stop("Col dendrogram is the wrong size")
        }
    }
    else {
        if (!is.null(Colv) && !is.na(Colv) && !identical(Colv, 
                                                         FALSE)) {
            warning("Invalid value for Colv, ignoring")
        }
        Colv <- NULL
        colInd <- 1:nc
    }
    if (missing(revC)) {
        if (symm) {
            revC <- TRUE
        }
        else if (is.dendrogram(Colv) & is.dendrogram(Rowv) & 
                 identical(Rowv, rev(Colv))) {
            revC <- TRUE
        }
        else {
            revC <- FALSE
        }
    }
    if (revC) {
        Colv <- rev(Colv)
        colInd <- rev(colInd)
    }
    if (is.null(cellnote)) 
        cellnote <- x
    if (is.numeric(cellnote)) {
        cellnote <- round(cellnote, digits = digits)
    }
    x <- x[rowInd, colInd, drop = FALSE]
    cellnote <- cellnote[rowInd, colInd, drop = FALSE]
    if (!is.null(point_size_mat)) {
        point_size_mat <- point_size_mat[rowInd, colInd, drop = FALSE]
    }
    if (!is.null(custom_hovertext)) {
        custom_hovertext <- custom_hovertext[rowInd, colInd, 
                                             drop = FALSE]
    }
    if (!is.null(row_side_colors)) {
        if (!(is.data.frame(row_side_colors) | is.matrix(row_side_colors))) {
            row_side_colors <- data.frame(row_side_colors = row_side_colors)
        }
        assert_that(nrow(row_side_colors) == nrow(x))
        row_side_colors <- row_side_colors[rowInd, , drop = FALSE]
    }
    if (!is.null(col_side_colors)) {
        if (!(is.data.frame(col_side_colors) | is.matrix(col_side_colors))) {
            col_side_colors <- data.frame(col_side_colors)
            colnames(col_side_colors) <- "col_side_colors"
        }
        assert_that(nrow(col_side_colors) == ncol(x))
        col_side_colors <- col_side_colors[colInd, , drop = FALSE]
    }
    dendextend::assign_dendextend_options()
    if (is.dendrogram(Rowv)) {
        if (is.na(k_row)) 
            k_row <- find_k(Rowv)$k
        if (k_row > 1) {
            # consider changing color-scheme for clusters
            Rowv <- color_branches(Rowv, k = k_row, col = heatmaply:::k_colors(k_row))
        }
    }
    if (is.dendrogram(Colv)) {
        if (is.na(k_col)) 
            k_col <- find_k(Colv)$k
        if (k_col > 1) {
            # consider changing color-scheme for clusters
            Colv <- color_branches(Colv, k = k_col, col = heatmaply:::k_colors(k_col))
        }
    }
    rowDend <- if (is.dendrogram(Rowv)) 
        Rowv
    else NULL
    colDend <- if (is.dendrogram(Colv)) 
        Colv
    else NULL
    if (!is.null(cellnote)) {
        if (is.null(dim(cellnote))) {
            if (length(cellnote) != nr * nc) {
                stop("Incorrect number of cellnote values")
            }
            dim(cellnote) <- dim(x)
        }
        if (!identical(dim(x), dim(cellnote))) {
            stop("cellnote matrix must have same dimensions as x")
        }
    }
    if (is.data.frame(custom_hovertext)) {
        custom_hovertext <- as.matrix(custom_hovertext)
    }
    mtx <- list(data = as.matrix(x), cellnote = cellnote, dim = dim(x), 
                rows = rownames(x), cols = colnames(x), point_size_mat = point_size_mat, 
                custom_hovertext = custom_hovertext)
    if (is.factor(x)) {
        colors <- col_factor(colors, x, na.color = "transparent")
    }
    else {
        rng <- range(x, na.rm = TRUE)
        if (scale %in% c("row", "column")) {
            rng <- c(max(abs(rng)), -max(abs(rng)))
        }
        colors <- col_numeric(colors, rng, na.color = "transparent")
    }
    options <- NULL
    options <- c(options, list(xaxis_height = xaxis_height, yaxis_width = yaxis_width, 
                               xaxis_font_size = xaxis_font_size, yaxis_font_size = yaxis_font_size, 
                               brush_color = brush_color, show_grid = show_grid, anim_duration = anim_duration))
    if (is.null(rowDend)) {
        c(options, list(yclust_width = 0))
    }
    if (is.null(colDend)) {
        c(options, list(xclust_height = 0))
    }
    heatmapr <- list(rows = rowDend, cols = colDend, matrix = mtx, 
                     theme = theme, options = options, cellnote = cellnote)
    if (!is.null(row_side_colors)) 
        heatmapr[["row_side_colors"]] <- row_side_colors
    if (!is.null(col_side_colors)) 
        heatmapr[["col_side_colors"]] <- col_side_colors
    class(heatmapr) <- "heatmapr"
    heatmapr
}


#--------------
# FUNCTION
#    plotlyPDF
# COMMENT
#    this function is a local alternative to orca() <- cause this costs money
#    install_phantomjs is commented out because it is finicky on non-linux systems
# REQUIRES
#    webshot
#    htmlwidgets
# VARIABLES
#    p = the plotly plot
#    name = the filename to save to (exclude file extension)
#    delay = how long to wait (in seconds) for plot to render before generating pdf
#    ... = any variables passed to webshot
# RETURNS
#    a pdf and html of the plotly plot
# plotlyPDF <- function(p, name = "name", delay = 5, ...){
#     #if(!file.exists("~/bin/phantomjs")){
#     #    webshot::install_phantomjs()
#     #}
#     saveWidget(p, paste0(name, ".html"), selfcontained = TRUE)
#     webshot(url = paste0(name, ".html"), file = paste0(name, ".pdf"), delay = delay, ...)
#     
# }

#--------------
# FUNCTION
#    listIntersect
# COMMENT
#    this is a helper function for venn.diagram2. it generates all the sets needed
#    for the draw.x.venn functions
# REQUIRES
#    magrittr
# VARIABLES
#    x = a list (possibly named) of genes (or anything that can be matched) for being passed
#    to venn.diagram2
# RETURNS
#    all possible intersections of list elements
# setList <- list(listA = c("A", "B", "C"), listB = c("D","E","A"), listC = c("B","C","X"))
# listIntersect <- function(x){
#     if(length(x) >= 2 && length(x) <= 5){
#         modes <- vector(mode = "list", length = length(x) - 2)
#         for(i in length(x):2){
#             modes[[i-1]] <- t(combn(length(x), i))
#         }
#     } else if(length(x) == 1){
#         return(length(x[[1]]))
#     } else {
#         stop("Inappropriate number of sets to intersect")
#     }
#     
#     out <- 
#         lapply(modes, nrow) %>% 
#         unlist() %>% 
#         sum() %>% 
#         vector(mode = "list", length = .)
#     
#     k <- 1
#     for(i in 1:length(modes)){
#         for(j in 1:nrow(modes[[i]])){
#             out[[k]] <- Reduce(intersect, x[modes[[i]][j,]])
#             names(out)[k] <- paste("n", paste(modes[[i]][j,], collapse = ""), sep = "")
#             k <- k+1
#         }
#     }
#     return(out)
# }

#' a wrapper for ggvenn that also keep set contents 
#' @param x a named list of character vectors that the set analysis will be performed on or a truth table
#' a binary table with sets in columns and elements in rows. If **x** is the truth table
#' *column* argument of *ggvenn()* must be set to indicate the columns to be used in the analysis 
#' @seealso ggvenn::ggvenn() 
#' @param set_name_size a ggvenn parameter setting the size of the set label 
#' @text_size a ggven parameter setting text size for intersect contents 
#' @nameLookUp a two-column data frame with the fist column containing names(x) and the second column - $names contains
#' set names to be displayed. If NULL names(x) will be used
#' @import ggvenn
#' @import dplyr
#' @import  tibble
#' @import  ggplot2
#' @variabes 
#'    all arguments are the same as in the original venn.diagram. 
#'    NB! This function was tested only with named list as the main argument
#' @returns 
#'   listOut <- list (ggplotVenn     =  plotPoly,
#'                     vennPartitions = vennPartitions)
#'    $ggplotVenn is a ggplot of a Venn diagram
#'    $vennPartitions is a list of elements in each Venn diagram subset   
venn.diagram2 <- function (x,
                           set_name_size = 6,
                           text_size = 6,
                           nameLookUp = NULL,
                           fill_color=c("#E64B35FF", "#4DBBD5FF", "#00A087FF", "#3C5488FF", "#F39B7FFF"), 
                           ...) {

    if (0 == length(x) | length(x) > 4) {
        warning("More then 4 sets cannot be shown. Only first 4 sets will be selected, Use upset plot for a larger number of sets", call. = FALSE)
    }
    
    # So.... I will cheat if NameLookUp is NULL I create it  here
    if(is.null(nameLookUp)){
        nameLookUp = data.frame(originalNames = names(x), names = names(x) )
    }
    
    annotationTable <- nameLookUp[nameLookUp$names %in% names(x),]
    # contrast_names <- paste0("C", seq(length(x)))
    # annotationTable$names <- names(x) <- contrast_names
    
    plotPoly <- ggvenn::ggvenn(data = x, 
                               fill_color = fill_color, 
                               show_percentage = F, 
                               stroke_color = NA,
                               set_name_size = set_name_size,
                               text_size = text_size) + 
        scale_y_continuous(expand = expansion(add = c(1,1))) +
        scale_x_continuous(expand = expansion(add = c(1,1))) 
    
    plotTable <- tableGrob(d = annotationTable, rows = NULL, 
                           theme = ttheme_default(base_size = 12))
    
    plotPoly = plotPoly / plotTable + patchwork::plot_layout(heights = c(4, 1))
    
    vennPartitions <- calculate.overlap2(x) 
    
    vennDF <- getSetTable(x)    
    
    listOut <- list (ggplotVenn = plotPoly, 
                     vennPartitions = vennPartitions, 
                     vennDF = vennDF)  
}


#' Function that extract elements of each subset
#' 
#' @param set a named list of sets to be intersected
#' 
#' @export
getSetTable <- function(set){
    
    VennDF <- get.venn.partitions(set)
   
    vennDFTerms <- 
        VennDF %>% 
        .[,c(ncol(.), 1:(ncol(.)-1))] %>% 
        dplyr::select(Counts = '..count..', dplyr::everything(), -c('..set..', '..values..'))
    
    vennDF <- 
        sapply(X = VennDF$..values.., FUN = toString) %>% 
        cbind(vennDFTerms, Genes = .)
    
    return(vennDF)
}

#--------------
# FUNCTION
#    this is to replace calclulate.overlap() from VennDiagram package. 
# COMMENT
#    as per descriptionn calculate.overlap "mostly complements the venn.diagram() 
#    function for the case where users want to know what values are grouped 
#    into the particular areas of the venn diagram."
#    The original version of this function does NOT return non-overlapping subsets for N=2. 
#    Instead it returns the entire lists
# REQUIRES
#    heatmaply, dendextend, seriation, assertthat
# VARIABLES
#    all the same as heatmapr
# RETURNS
#    a heatmapr object to be passed to heatmaply
calculate.overlap2 <- function (x) {
    if (1 == length(x)) {
        overlap <- x
    }
    else if (2 == length(x)) {
        ai <- intersect(x[[1]], x[[2]])
        overlap <- list(a1 = setdiff(x[[1]], ai), a2 = setdiff(x[[2]], ai), a3 = ai)
    }
    else if (3 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        nab <- intersect(A, B)
        nbc <- intersect(B, C)
        nac <- intersect(A, C)
        nabc <- intersect(nab, C)
        a5 = nabc
        a2 = nab[which(!nab %in% a5)]
        a4 = nac[which(!nac %in% a5)]
        a6 = nbc[which(!nbc %in% a5)]
        a1 = A[which(!A %in% c(a2, a4, a5))]
        a3 = B[which(!B %in% c(a2, a5, a6))]
        a7 = C[which(!C %in% c(a4, a5, a6))]
        overlap <- list(a5 = a5, a2 = a2, a4 = a4, a6 = a6, a1 = a1, 
                        a3 = a3, a7 = a7)
    }
    else if (4 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        D <- x[[4]]
        n12 <- intersect(A, B)
        n13 <- intersect(A, C)
        n14 <- intersect(A, D)
        n23 <- intersect(B, C)
        n24 <- intersect(B, D)
        n34 <- intersect(C, D)
        n123 <- intersect(n12, C)
        n124 <- intersect(n12, D)
        n134 <- intersect(n13, D)
        n234 <- intersect(n23, D)
        n1234 <- intersect(n123, D)
        a6 = n1234
        a12 = n123[which(!n123 %in% a6)]
        a11 = n124[which(!n124 %in% a6)]
        a5 = n134[which(!n134 %in% a6)]
        a7 = n234[which(!n234 %in% a6)]
        a15 = n12[which(!n12 %in% c(a6, a11, a12))]
        a4 = n13[which(!n13 %in% c(a6, a5, a12))]
        a10 = n14[which(!n14 %in% c(a6, a5, a11))]
        a13 = n23[which(!n23 %in% c(a6, a7, a12))]
        a8 = n24[which(!n24 %in% c(a6, a7, a11))]
        a2 = n34[which(!n34 %in% c(a6, a5, a7))]
        a9 = A[which(!A %in% c(a4, a5, a6, a10, a11, a12, a15))]
        a14 = B[which(!B %in% c(a6, a7, a8, a11, a12, a13, a15))]
        a1 = C[which(!C %in% c(a2, a4, a5, a6, a7, a12, a13))]
        a3 = D[which(!D %in% c(a2, a5, a6, a7, a8, a10, a11))]
        overlap <- list(a6 = a6, a12 = a12, a11 = a11, a5 = a5, 
                        a7 = a7, a15 = a15, a4 = a4, a10 = a10, a13 = a13, 
                        a8 = a8, a2 = a2, a9 = a9, a14 = a14, a1 = a1, a3 = a3)
    }
    else if (5 == length(x)) {
        A <- x[[1]]
        B <- x[[2]]
        C <- x[[3]]
        D <- x[[4]]
        E <- x[[5]]
        n12 <- intersect(A, B)
        n13 <- intersect(A, C)
        n14 <- intersect(A, D)
        n15 <- intersect(A, E)
        n23 <- intersect(B, C)
        n24 <- intersect(B, D)
        n25 <- intersect(B, E)
        n34 <- intersect(C, D)
        n35 <- intersect(C, E)
        n45 <- intersect(D, E)
        n123 <- intersect(n12, C)
        n124 <- intersect(n12, D)
        n125 <- intersect(n12, E)
        n134 <- intersect(n13, D)
        n135 <- intersect(n13, E)
        n145 <- intersect(n14, E)
        n234 <- intersect(n23, D)
        n235 <- intersect(n23, E)
        n245 <- intersect(n24, E)
        n345 <- intersect(n34, E)
        n1234 <- intersect(n123, D)
        n1235 <- intersect(n123, E)
        n1245 <- intersect(n124, E)
        n1345 <- intersect(n134, E)
        n2345 <- intersect(n234, E)
        n12345 <- intersect(n1234, E)
        a31 = n12345
        a30 = n1234[which(!n1234 %in% a31)]
        a29 = n1235[which(!n1235 %in% a31)]
        a28 = n1245[which(!n1245 %in% a31)]
        a27 = n1345[which(!n1345 %in% a31)]
        a26 = n2345[which(!n2345 %in% a31)]
        a25 = n245[which(!n245 %in% c(a26, a28, a31))]
        a24 = n234[which(!n234 %in% c(a26, a30, a31))]
        a23 = n134[which(!n134 %in% c(a27, a30, a31))]
        a22 = n123[which(!n123 %in% c(a29, a30, a31))]
        a21 = n235[which(!n235 %in% c(a26, a29, a31))]
        a20 = n125[which(!n125 %in% c(a28, a29, a31))]
        a19 = n124[which(!n124 %in% c(a28, a30, a31))]
        a18 = n145[which(!n145 %in% c(a27, a28, a31))]
        a17 = n135[which(!n135 %in% c(a27, a29, a31))]
        a16 = n345[which(!n345 %in% c(a26, a27, a31))]
        a15 = n45[which(!n45 %in% c(a18, a25, a16, a28, a27, 
                                    a26, a31))]
        a14 = n24[which(!n24 %in% c(a19, a24, a25, a30, a28, 
                                    a26, a31))]
        a13 = n34[which(!n34 %in% c(a16, a23, a24, a26, a27, 
                                    a30, a31))]
        a12 = n13[which(!n13 %in% c(a17, a22, a23, a27, a29, 
                                    a30, a31))]
        a11 = n23[which(!n23 %in% c(a21, a22, a24, a26, a29, 
                                    a30, a31))]
        a10 = n25[which(!n25 %in% c(a20, a21, a25, a26, a28, 
                                    a29, a31))]
        a9 = n12[which(!n12 %in% c(a19, a20, a22, a28, a29, a30, 
                                   a31))]
        a8 = n14[which(!n14 %in% c(a18, a19, a23, a27, a28, a30, 
                                   a31))]
        a7 = n15[which(!n15 %in% c(a17, a18, a20, a27, a28, a29, 
                                   a31))]
        a6 = n35[which(!n35 %in% c(a16, a17, a21, a26, a27, a29, 
                                   a31))]
        a5 = E[which(!E %in% c(a6, a7, a15, a16, a17, a18, a25, 
                               a26, a27, a28, a31, a20, a29, a21, a10))]
        a4 = D[which(!D %in% c(a13, a14, a15, a16, a23, a24, 
                               a25, a26, a27, a28, a31, a18, a19, a8, a30))]
        a3 = C[which(!C %in% c(a21, a11, a12, a13, a29, a22, 
                               a23, a24, a30, a31, a26, a27, a16, a6, a17))]
        a2 = B[which(!B %in% c(a9, a10, a19, a20, a21, a11, a28, 
                               a29, a31, a22, a30, a26, a25, a24, a14))]
        a1 = A[which(!A %in% c(a7, a8, a18, a17, a19, a9, a27, 
                               a28, a31, a20, a30, a29, a22, a23, a12))]
        overlap <- list(a31 = a31, a30 = a30, a29 = a29, a28 = a28, 
                        a27 = a27, a26 = a26, a25 = a25, a24 = a24, a23 = a23, 
                        a22 = a22, a21 = a21, a20 = a20, a19 = a19, a18 = a18, 
                        a17 = a17, a16 = a16, a15 = a15, a14 = a14, a13 = a13, 
                        a12 = a12, a11 = a11, a10 = a10, a9 = a9, a8 = a8, 
                        a7 = a7, a6 = a6, a5 = a5, a4 = a4, a3 = a3, a2 = a2, 
                        a1 = a1)
    }
    else {

        stop("Invalid size of input object")
    }
}


#--------------
# FUNCTION
#    geneFromPathway
# COMMENT
#    A helper function that extract genes names for a specific Pathways and a contrast from QuSage output in KOvWT objec
# REQUIRES
#    stringi
#    
# VARIABLES
#    geneSet = the name of a gene set of interest eg,"REACTOME_NOTCH1_INTRACELLULAR_DOMAIN_REGULATES_TRANSCRIPTION"
#              will accept the vector of pathway names    
# RETURNS
#    a vector of gene names for a pathways (or a set of pathways) 
# geneFromPathway <- function(geneSet){ 
#     if(!is.vector(geneSet) | length(geneSet) == 0)stop("geneSet is either not a vector of empty")
#     qsageIndex=grep("quSage",names(KOvWT))
#     pathIndex <- sapply(geneSet,function(x) KOvWT[[qsageIndex[1]]]$PathwayIndexs[names(KOvWT[[qsageIndex[1]]]$PathwayIndexs) == x])
#     pathGenes <- lapply(pathIndex, function(x)stringi::stri_trans_totitle(rownames(KOvWT[[qsageIndex[1]]]$GeneTable)[x]) ) %>% unlist %>% unique
#     return(pathGenes)
# }

#--------------
# FUNCTION
#    pathwayHeatmapFDR
# COMMENT
#    A helper function that performs K mean clustering of quSage FDR/Pvalues, creates a heatmap 
# using ComplexHeatmap package and  returns anobject that contains a ggplot object with a heatmap, 
# list of pathways for each clusters with associated pvalues/FDR and a least of genes for each identified pathway
# REQUIRES
#    "dplyr", "tibble", "reshape2", "ComplexHeatmap","circlize"
#    
# VARIABLES
#    kovwtObj - the name of KOvWT object. At the moment it is comented out and the use of KOvWT is hardwired)
#    km - the number of clusters for K-mean clustering (default =1)
#    clustering_distance_rows - distance method for gene clustering. The following distance 
#      (provided by ComplexHeatmap package) are available, "euclidean", "maximum", "manhattan",
#      "canberra", "binary", "minkowski", "pearson", "spearman", "kendall" 
#    clustering_method_rows - clustering methods for genes. THe following methods are available
#      "ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"
#    fdrCutOff - "significance" cutoff for FDR/P value (default = 0.01)
#    minPathSize - the smallest number of genes in a pathway (default =10)
#    FDR - logical. IF TRUE FDR will be used as a "significance" metric otherwise the PValue will be used
# RETURNS
#    a list with four elements
#    `Heatmap of FDR values` - well, a heatmap
#    `Pathways in clusters` - if K mean clustering was used  a list of clusters with pathways
#    `Genes in selected pathways` - genes that belong to each pathways selected for the analysis and
#                                   shown on Heatmap
#    `Analysis constraints` - parameters of cluster analysis such as Clustering distance,
# Clustering method, metric (FDR or Pvalue),metric CutOff,Minimal number of genes in pathway,
# and the number of K mean clusters
# 
# EXAMPLE:
# Assuming KOvWT object if present in the memory
#
# test <- pathwayHeatmapFDR(km = 5, clustering_distance_rows = "minkowski", clustering_method_rows = "ward.D2", FDR=T)

# pathwayHeatmapFDR <- function(
#     #kovwtObj=NULL,
#     km=1,
#     clustering_distance_rows = c("euclidean", "maximum", "manhattan", "canberra", "binary", "minkowski", "pearson", "spearman", "kendall"),
#     clustering_method_rows = c("ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"),
#     fdrCutOff = 0.01,
#     minPathSize =10,
#     FDR = T){
#     
#     if (length(clustering_distance_rows)!=1){warning("Clustering distance for pathways has not being selected. Set to \"minkowski\""); clustering_distance_rows = "minkowski"}
#     if (length(clustering_method_rows)!=1){warning("Clustering method for pathways has not being selected. Set to \"ward.D2\""); clustering_method_rows = "ward.D2"}    
#     
#     qusageIndex <-  grep("quSage",names(KOvWT)) # indexes of all qsage entries in the object
#     
#     metric <- ifelse(FDR,"FDR","PValue")
#     
#     allPathwayFDR <-  sapply(qusageIndex,function(x)dplyr::select(KOvWT[[x]][[2]], metric), simplify=F,USE.NAMES = T) %>%
#         as.data.frame %>% tibble::rownames_to_column(var = "rowname") %>%
#         cbind.data.frame(.,size=KOvWT[[length(KOvWT)]][[2]]$size) %>%
#         filter_at(vars(contains(metric)),any_vars(.<fdrCutOff)) %>%
#         filter(.,size>minPathSize) %>%
#         mutate_all(~replace(., is.na(.), 1)) %>%
#         replace(., .== 0, min(.[,2:(ncol(.)-1)]) ) %>%
#         filter(.,nchar(as.character(rowname))>10) %>% # removing strange short names
#         select(.,-ncol(.)) %>%                        # remove size - it messes up heatmap
#         mutate_at(.,vars(contains(metric)), ~(-log10(.))) %>%
#         tibble::column_to_rownames(., var = "rowname") %>%
#         set_colnames(.,colnames(KOvWT$contrastMatrix))
#     
#     hm2<- Heatmap(allPathwayFDR,
#                   col = colorRamp2(c(min(allPathwayFDR),max(allPathwayFDR)), c("white" ,"darkred")),
#                   #border = T,
#                   show_row_names = F, 
#                   row_title_rot = 0,
#                   clustering_distance_rows = clustering_distance_rows,
#                   clustering_method_rows = clustering_method_rows,
#                   cluster_columns = T , 
#                   #cluster_row=T,
#                   gap=unit(3, "mm"),
#                   name = "-Log10(FDR)",
#                   #rect_gp = gpar(type="none"),
#                   #km_title = "cl%i",
#                   km=km)
#     set.seed(123)
#     hm2 <- draw(hm2) 
#     
#     clusters_allPathway <- lapply(ComplexHeatmap::row_order(hm2),function(x)allPathwayFDR[x,])
#     
#     selectedPath <- names(KOvWT[[qusageIndex[1]]][[3]]) %in% rownames(allPathwayFDR)
#     
#     genes_in_Pathway <- sapply(KOvWT[[qusageIndex[1]]][[3]][selectedPath],function(x)toupper(KOvWT$topTagTable$gene_name[x]),simplify = F, USE.NAMES = T)
#     
#     return(list(`Heatmap of FDR values` = hm2, 
#                 `Pathways in clusters` = clusters_allPathway, 
#                 `Genes in selected pathways` = genes_in_Pathway, 
#                 `Analysis constraints` = list( `Clustering distance` = clustering_distance_rows,
#                                                `Clustering method` = clustering_method_rows,
#                                                `Metric` = metric,
#                                                `Metric CutOff` = fdrCutOff,
#                                                `Minimal number of genes in pathway` = minPathSize,
#                                                `Number of K mean clusters ` = km
#                 )))
#     
# }

######################################################################################################

# FUNCTION
#    pathwayHeatmapFC
# COMMENT
#    A helper function that performs K mean clustering of quSage FoldChanges, creates a heatmap 
# using ComplexHeatmap package and  returns an object that contains a ggplot object with a heatmap, 
# list of pathways for each clusters with associated pvalues/FDR and a least of genes for each identified pathway
# This function is very similarl to pathwayHeatmapFDR
#
# REQUIRES
#    "dplyr", "tibble", "reshape2", "ComplexHeatmap","circlize"
#    
# VARIABLES
#    kovwtObj - the name of KOvWT object. At the moment it is comented out and the use of KOvWT is hardwired)
#    km - the number of clusters for K-mean clustering (default =1)
#    clustering_distance_rows - distance method for gene clustering. The following distance 
#      (provided by ComplexHeatmap package) are available, "euclidean", "maximum", "manhattan",
#      "canberra", "binary", "minkowski", "pearson", "spearman", "kendall" 
#    clustering_method_rows - clustering methods for genes. THe following methods are available
#      "ward.D2", "single", "complete", "average", "mcquitty", "median","centroid"
#    fdrCutOff - "significance" cutoff for FDR/P value (default = 0.01)
#    minPathSize - the smallest number of genes in a pathway (default =10)
#    FDR - logical. IF TRUE FDR will be used as a "significance" metric otherwise the PValue will be used
#    FCCutOff - logFC cutoff for the pathway. Set to 0 if you want ALL significant pathways
# RETURNS
#    a list with four elements
#    `Heatmap of FDR values` - well, a heatmap
#    `Pathways in clusters` - if K mean clustering was used  a list of clusters with pathways
#    `Genes in selected pathways` - genes that belong to each pathways selected for the analysis and
#                                   shown on Heatmap
#    `Analysis constraints` - parameters of cluster analysis such as Clustering distance,
# Clustering method, metric (FDR or Pvalue),metric CutOff,Minimal number of genes in pathway,
# and the number of K mean clusters
# 
# EXAMPLE:
# Assuming KOvWT object if present in the memory
#
# test <- pathwayHeatmapFC(kovwtObj = KOvWT,km = 6,clustering_distance_rows = "euclidean",clustering_method_rows = "ward.D2", FCCutOff = 0.3) 

#' 
#' 
#' @description 
#' 
#' @param 
#' 
#' @import dplyr
#' @import tibble
#' @import ComplexHeatmap
#' @import circlize
#' 
# pathwayHeatmapFC <- function(kovwtObj = NULL,
#                              clustering_distance_rows = c("euclidean", "maximum", "manhattan", 
#                                                           "canberra", "binary", "minkowski",
#                                                           "pearson", "spearman", "kendall"),
#                              clustering_method_rows = c("ward.D2", "single", "complete", 
#                                                         "average", "mcquitty", "median",
#                                                         "centroid"),
#                              km = 1,
#                              FCCutOff = 0.5,
#                              fdrCutOff = 0.01,
#                              FDR = T,
#                              minPathSize = 10) {
#     
#     if(length(clustering_distance_rows) != 1){
#         warning("Clustering distance for pathways has not being selected. Set to \"euclidean\"")
#         clustering_distance_rows <- "euclidean"
#     }
#     if(length(clustering_method_rows) != 1){
#         warning("Clustering method for pathways has not being selected. Set to \"ward.D2\"")
#         clustering_method_rows <- "ward.D2"
#     }    
#     
#     metric <- ifelse(FDR, "FDR", "PValue")
#     
#     qusageIndex <-  grep("quSage", names(KOvWT)) # indexes of all qsage entries in the object
#     
#     allPathwayFC <-  sapply(qusageIndex, function(x) dplyr::select(KOvWT[[x]][[2]], c("logFC", metric)), 
#                             simplify = F, USE.NAMES = T) %>% 
#         as.data.frame %>% tibble::rownames_to_column(var = "rowname") %>%  
#         cbind.data.frame(.,size=KOvWT[[length(KOvWT)]][[2]]$size) %>%
#         filter_at(vars(contains(metric)),any_vars(.<fdrCutOff)) %>%    
#         filter_at(vars(contains("logFC")),any_vars(abs(.)>FCCutOff)) %>% 
#         filter(.,size>minPathSize) %>%
#         select(.,-contains("FDR")) %>% 
#         mutate_all(~replace(., is.na(.),0 ) ) %>%  
#         filter(.,nchar(as.character(rowname))>10) %>% # removing strange short names
#         select(.,-ncol(.)) %>%                        # remove size - it messes up heatmap 
#         tibble::column_to_rownames(., var = "rowname") %>% 
#         set_colnames(.,colnames(KOvWT$contrastMatrix))
#     
#     hm2<- Heatmap(allPathwayFC,
#                   col = colorRamp2(c(min(allPathwayFC),0, max(allPathwayFC)), c("blue", "white" , "red")),
#                   #border = T,
#                   show_row_names = F, 
#                   row_title_rot = 0,
#                   clustering_distance_rows = clustering_distance_rows,
#                   clustering_method_rows = clustering_method_rows,
#                   cluster_columns = T , 
#                   #cluster_row=T,
#                   gap=unit(3, "mm"),
#                   name = "Log2FC",
#                   #rect_gp = gpar(type="none"),
#                   #km_title = "cl%i",
#                   km=km)
#     set.seed(123)
#     hm2 <- draw(hm2) 
#     
#     clusters_allPathway <- lapply(ComplexHeatmap::row_order(hm2),function(x)allPathwayFC[x,])
#     
#     selectedPath <- names(KOvWT[[qusageIndex[1]]][[3]]) %in% rownames(allPathwayFC)
#     
#     genes_in_Pathway <- sapply(KOvWT[[qusageIndex[1]]][[3]][selectedPath],function(x)toupper(KOvWT$topTagTable$gene_name[x]),simplify = F, USE.NAMES = T)
#     
#     #heatmapPlot <- 
#     #    heatmaply(x = allPathwayFC, 
#     #              colors =  c("blue", white" ,"dred"),
#     #              margins = c(200, 400, 50, 0),
#     #              plot_method = "plotly", 
#     #              labCol = colnames(allPathwayFC), 
#     #              labRow = rownames(allPathwayF), 
#     #              dist_method = clustering_distance_rows,
#     #              hclust_method = clustering_method_rows,
#     #              dendrogram = c("both"),
#     #              fontsize_row = 6
#     #key.title = switch(input$geneExprHeatScale, none = "logCPM", row = "Z-Score"),
#     #scale = input$geneExprHeatScale,
#     #col_side_colors = heatCovariates, 
#     #             )
#     
#     return(list(`Heatmap of FC values` = hm2, 
#                 `Pathways in clusters` = clusters_allPathway, 
#                 `Genes in selected pathways` = genes_in_Pathway, 
#                 `Analysis constraints` = list( `Clustering distance` = clustering_distance_rows,
#                                                `Clustering method` = clustering_method_rows,
#                                                `Metric` = metric,
#                                                `Metric CutOff` = fdrCutOff,
#                                                `Abs(Fold Change CutOff)` = FCCutOff,
#                                                `Minimal number of genes in pathway` = minPathSize,
#                                                `Number of K mean clusters ` = km
#                 )))
#     
#     
# }


#--------------
# FUNCTION
#    plotlyText
# COMMENT
#    A convenience function for pretty text errors
# REQUIRES
#    plotly
# VARIABLES
#    displayText = a string that you would like displayed in the plotting area
# RETURNS
#    Nothing, used for it's side effect of plotting
plotlyText <- function(displayText) {
    plot_ly(data.frame(x = 0, y = 0), x = ~x, y = ~y,
            type = "scatter", 
            mode = "text", 
            text = displayText,
            textfont = list(color = '#000000', size = 16)) %>% 
        layout(xaxis = list(visible = F),
               yaxis = list(visible = F)) %>% 
        config(displayModeBar = FALSE)
}



#'  A convenience function to generate pretty text errors
#' @param  displayText  a string that you would like displayed in the plotting area
#' @import  ggplot
#' @return Nothing, used for it's side effect of plotting
#' @export
plotText <- function(displayText){
    p <- ggplot() + 
        annotate("text", x = 4, y = 25, size = 8, label = displayText) + 
        theme_void() 
    print(p)
}


#--------------
# FUNCTION
#    buildTFNet -- Given a topTagTable, build a network appropriate data structure for tfVizNet
# COMMENT
#    
# REQUIRES
#    RColorBrewer
# VARIABLES
#    x = a topTagTable presumably from out_list$topTagTable
#    tfDb = a transcription factor database with columns from, to, and species
# RETURNS
#    a list of length 3 containging $nodeInfo, $edgeInfo, and $flag
buildTFNet <- function(x, tfDb, 
                       addNodeInfo = NULL, 
                       species = "any", 
                       removeSelfLoops = FALSE, 
                       nnOnly = FALSE,
                       nodeColorColumn, 
                       colorBins){
    # make sure node and edge infor are all uppercase
    tfDb$from <- toupper(tfDb$from)
    tfDb$to <- toupper(tfDb$to)
    x$id <- toupper(x$id)
    flag = FALSE # initally set the flag to false
    
    if(removeSelfLoops){
      tfDb <- tfDb[tfDb$from != tfDb$to,]
    }
    
    if(is.null(x) || !"id" %in% colnames(x)){
        stop("Please supply a TF-Target node table with at least an 'id' column")
    }
    
    if(is.null(tfDb) || !all(c("from", "to", "species") %in% colnames(tfDb))){
        stop("Please supply a TF-Target edge table with at least 'from', 'to', and 'species' columns")
    }
    
    if(nnOnly && nrow(x) == 1){
        # for nearest neighbors search only in `from`
        testNetExist <- 
            toupper(tfDb$from) %in% toupper(x$id) &
            switch(species,
                   Hs = { tfDb$species == "h" },
                   Mm = { tfDb$species == "m" },
                   any = { tfDb$species != "any" })
        # for nearest neighbors search we need to get additional node information
        if(!is.null(addNodeInfo)){
            addNodeInfo$id <- toupper(addNodeInfo$gene_name)
            addNodes <- tfDb[testNetExist, 1:2] %>% unlist(use.names = F) %>% unique()
            x <- addNodeInfo[addNodeInfo$id %in% addNodes,]
        }
    } else if(!nnOnly && nrow(x) != 1) {
        # for standard network require both to and from
        testNetExist <- 
            toupper(tfDb$from) %in% toupper(x$id) &
            toupper(tfDb$to) %in% toupper(x$id) &
            switch(species,
                   Hs = { tfDb$species == "h" },
                   Mm = { tfDb$species == "m" },
                   any = { tfDb$species != "any" })
    } else if(nnOnly && nrow(x) != 1){
        # if conflicting inputs, return an error generating node, edge set.
        nodes <- data.frame(id = 1:6)
        edges <- data.frame(from = rep(1, 5), to = c(2:6))
        flag = TRUE
    }
    
    if(any(testNetExist)){
        # if a valid network exists store only edge info related to that network
        edges <- tfDb[testNetExist,]
        if(species != "any"){
            edgeSpecies <- switch(species,
                                  Hs = { "h" },
                                  Mm = { "m" })
            edges <- edges[edges$species == edgeSpecies,]
        }
        # filter the node information to only connected nodes
        toGetNodes <- edges[,1:2] %>% unlist(use.names = F)
        x <- x[x$id %in% toGetNodes, ]
        # build nodes data.frame
        nodes <- x[, "id", drop = F ]
        nodes$label <- x[, "id"]
        nodes$TF <- nodes$id %in% edges$from
        nodes$Target <- nodes$id %in% edges$to
        nodes$TFT <- nodes$TF & nodes$Target
        nodes$group <- "Target"
        nodes$group[nodes$TF] <- "TF"
        nodes$group[nodes$TFT] <- "TFT"
        
        if(!is.null(nodeColorColumn)){
            nodes$color <- x[, nodeColorColumn]
            nodes[, nodeColorColumn] <- nodes$color
            if(!is.null(colorBins)){
                if(all(nodes$color < 1 & nodes$color > 0)){ # surrogate for guessing P-value or logFC
                    initPal <- rev(brewer.pal(11, "RdBu")[1:6])
                    nodes$color <- -log10(nodes$color)
                    finalPalFun <- colorRampPalette(initPal) 
                    names(colorBins) <- finalPalFun(256)
                } 
                nodes$color <- names(colorBins)[findInterval(nodes$color, colorBins, all.inside = T)]
            }
        } 
        if(isTRUE(nnOnly)){
            # visNetwork is fine with the nn network, but tidygraph chokes on 
            # edges that are non-existent. 
            edges <- edges[edges$to %in% nodes$id,]
        }
    } else {
        nodes <- data.frame(id = 1:6)
        edges <- data.frame(from = rep(1, 6), to = c(1:6)) 
        flag = TRUE
    }
    return(list(nodeInfo = nodes, edgeInfo = edges, flag = flag))
}

#--------------
# FUNCTION
#    tfVizNet -- builds a visNetwork plot for buildTFNet output.
# COMMENT
#    
# REQUIRES
#    visNetwork
# VARIABLES
#    networkData = output from buildTFNet
# RETURNS
#    a visNetwork plot
tfVizNet <- function(networkData, 
                     edges.smooth = TRUE, 
                     edges.selfRef = TRUE,
                     layout.hierarchical = FALSE, 
                     physics.solver = NULL,
                     physics.enable = TRUE) { # 
    
    if(networkData$flag){
        p <- visNetwork(networkData$nodeInfo, networkData$edgeInfo, 
                   main = list(text = "No TF <-> Target interactions\nidentified in your gene list.",
                               style = 'font-family:serif;font-weight:bold;font-size:36px;text-align:center;'), 
                   width = "100%")
        return(p)
    } else {
        lnodes = data.frame(label = c("TF+Target", "TF", "Target"),
                            shape = c("diamond", "triangle", "dot"),
                            color = c("#696969", "#696969", "#696969"),
                            size = c(10, 10, 10))
        if(edges.smooth){
            edges.smooth <- list(type = "dynamic")
        } else {
            edges.smooth <- list(enabled = FALSE)
        }

        p <- 
            visNetwork(networkData$nodeInfo, networkData$edgeInfo) %>% # , height = "1000px",width = "1000px"
            visNodes(font = list(size = 22, color = "#000000")) %>% 
            visEdges(arrows = "to", 
                     smooth = edges.smooth, 
                     selfReference = ifelsenull(!edges.selfRef),
                     color = list(color = "#696969", highlight = "#ff3333")) %>% 
            visPhysics(stabilization = FALSE, solver = physics.solver, enabled = physics.enable) %>% 
            visLayout(randomSeed = 11377, hierarchical = ifelsenull(layout.hierarchical)) %>% 
            visInteraction(multiselect = TRUE) %>% 
            visLegend(width = 0.2, stepY = 75, useGroups = F, addNodes = lnodes) %>% 
            visGroups(groupname = "TFT", shape = "diamond") %>% 
            visGroups(groupname = "TF", shape = "triangle") %>% 
            visGroups(groupname = "Target", shape = "dot") 
        return(p)
    }
}

#--------------
# FUNCTION
#    allPairwiseIntersect
# COMMENT
#    calculate pathway intersections
# REQUIRES
#    idk yet
# VARIABLES
#    x = a qusage object from out_list
# RETURNS
#    a network? maybe...
# allPairwiseIntersect <- function(x){
#     allIntersects <- combn(1:length(x$PathwayIndexs), 2) %>% t()
#     
#     allJaccList <- lapply(1:nrow(allIntersects), function(i){
#         jaccNum <- intersect(x$PathwayIndexs[[allIntersects[i,1]]], 
#                              x$PathwayIndexs[[allIntersects[i,2]]]) %>% length()
#         jaccDen <- union(x$PathwayIndexs[[allIntersects[i,1]]], 
#                          x$PathwayIndexs[[allIntersects[i,2]]]) %>% length()
#         jaccStat <- jaccNum/jaccDen
#         return(jaccStat)
#     })
#     
#     names(allJaccList) <- apply(allIntersects, 1, paste, collapse = "_")
#     
#     test <- unlist(allJaccList)
#     
#     S <- diag(length(x$PathwayIndexs))
#     S[lower.tri(S, diag = F)] <- test
#     diag(S) <- 0
#     S[upper.tri(S)] <- t(S)[upper.tri(S)]
#     isSymmetric(S)
#     
#     # G <- graph_from_adjacency_matrix(S, mode = "undirected", weighted = TRUE, diag = FALSE)
#     # L <- layout_(G, layout = with_dh())
#     # plot(G, layout = L, edge.width = E(G)$weight)
#     
# } 

#' A wrapper around a wrapper 
#' 
#' @description Allows delim to be set in str_wrap function
#' 
#' @param x a character string
#' @param delim A character to split the string on. Supports vector of delimiters
#' Default: " "
#' @param width a numeric value specifying the string width
#' Default: getOption("width")
#' @param ... Additional parameters passed to `stringr::str_wrap`
#' 
#' @import sringr
#' 
#' @export
str_wrap2 <- function(x, delim = " ", width = getOption("width"), ...){
  if(missing(x)){
    stop("x is missing with no defaults")
  }
  if(!is.character(delim)){
    stop("delim must be a character string")
  }
  if(!is.numeric(width)){
    stop("width must be numeric")
  }
  if(length(width) > 1){
    warning("multiple values passed to width, using the first value.")
    width <- width[1]
  }
  if(length(delim) > 1){
    paste0(delim, collapse = "|")
  }

  z <- stringr::str_split(x, delim) %>%  unlist() %>%  paste0(collapse = "") %>%
    stringr::str_wrap(width = width, ...)
  return(z)
}

#' @title GGplot theme based on theme_minimal
#' @description A preferred ggplot2 theme for our static figures.
#' @param angle45 logical, changes angle and alignment of X axis labels for long labels. 
#' @param base_size integer, Set to 14. Determine the font base_size for the theme
#' @note This theme hides legends - beware 
#' @note angle45 should always be set to FALSE when used with coord_flip()
#' @export

theme_HSS <- function (angle45 = FALSE, base_size = 14) {
  theme_minimal(base_size = base_size) %+replace%
    theme(
      panel.background = element_blank(),
      panel.grid = element_blank(),
      plot.background = element_rect(fill="white", colour=NA),
      legend.background = element_rect(fill="transparent", colour=NA),
      legend.key = element_rect(fill="transparent", colour = NA),
      axis.line = element_line(color = "#000000", size = 1),
      axis.ticks = element_line(color = "#000000"),
      axis.title = element_text(color = "#000000", size = 18),
      axis.text = element_text(color = "#000000", size = 16),
      axis.text.x = element_text(angle = ifelse(angle45, 45, 0),
                                 hjust = ifelse(angle45, 1, 0.5),
                                 vjust = 1),
      plot.title = element_text(face = "bold", size = 20, color = "#000000"),
      plot.subtitle = element_text(size = 16, color = "#000000")
    )
}

#' This function converts a list into a binary membership matrix with colums as list elements and
#' rows as list names. If list is not named we will name it for you, for what it's worth
#' @param .list a list
#' 
#' @return
#'   
listToTruthTable <- function(.list){
  
  if(length(.list) == 0){
    stop(".list must not be empty")
  }
  if(class(.list) != "list"){
    stop(".list must be a list")
  }
  if(is.null(names(.list))){
    warning(".list must be a named list. This one is not. Named will be assigned")
    names(.list) <- paste0("name",c(1:length(.list)))
  }
  df <- .list %>% tibble::enframe() %>% 
    tidyr::unnest(cols = c(value)) %>%
    dplyr::add_count(name, value) %>% data.frame() %>% 
    tidyr::pivot_wider(names_from = name, values_from = n, values_fill = 0)

    df[,c(2:ncol(df))] <- df[,c(2:ncol(df))] >= 1 
 
  return(df)
}

#` Make a truth table
#' 
#' Makes a truth table of the inputs. We use this function for counting set composition
#' @param:  x A short vector.
#' @return:  A data frame with length(x) logical vector columns and  2 ^ length(x) rows.
#' @seealso:  [base]{expand.grid}
#' @author:  Richard Cotton
#' @examples
#' make.truth.table(c(a = 1, b = 2, c = 3, d = 4))
#' @export
make.truth.table <- function(x)
{
  #Fix missing or duplicated names
  if(is.null(names(x)) || any(c(NA,"") %in% names(x)) || (length(unique(names(x))) != length(names(x))))
  {
    warning("fixing missing, empty or duplicated names.")
    nx <- if(is.null(names(x))) seq_along(x) else names(x)
    names(x) <- make.names(nx, unique = TRUE)
  }  
  
  tf <- lapply(seq_along(x), function(.) c(TRUE, FALSE))    
  setNames(do.call(expand.grid, tf), names(x))
}

#' Get the size of individual partitions in a Venn diagram
#' 
#' Partitions a list into Venn regions.
#' 
#' @param x A list of vectors.
#' @param force.unique A logical value.  Should only unique values be considered?
#' @param keep.elements A logical value. Should the elements in each region be returned?
#' @param hierarchical A logical value. Changed the way overlapping elements are treated if force.unique is TRUE.
#'
#' @return A data frame with length(x) columns and 2 ^ length(x) rows.  The first length(x) columns are all
#' logical; see {make.truth.table} for more details.  There are threeadditional columns: 
#' 
#' ..set..: {A set theoretical desription of the Venn region.  (Note that
#' in some locales under Windows, the data.frame print method fails to correctly
#' display the Unicode symbols for set union and set intersection.  This is a 
#' bug in R, not this function.)}
#' ..values..: {A vector of values contained in the Venn region. Not returned if keep.elements is FALSE.}
#' ..count..: {An integer of the number of values in the Venn region.}
#' 
#' @section Details
#' If force.unique is FALSE, then there are two supported methods of grouping categories with duplicated elements in common.
#' If hierarchical is FALSE, then any common elements are gathered into a pool. So if
#' x <- list(a = c(1,1,2,2,3,3), b=c(1,2,3,4,4,5), c=c(1,4)) then (b intersect c)/(a) would contain
#' three 4's. Since the 4's are pooled, (b)/(a union c) contains no 4's.
#' If hierachical is TRUE, then (b intersect c)/(a) would contain one 4.Then (b)/(a union c) cotains one 4.
#' 
#' @author Richard Cotton.
#' @seealso [VennDiagram]{make.truth.table}
#'
#' @examples
#' #Compare force.unique options
#' x <- list(a = c(1, 1, 1, 2, 2, 3), b = c(2, 2, 2, 3, 4, 4))
#' get.venn.partitions(x)
#' get.venn.partitions(x, force.unique = FALSE)
# 
#' # Figure 1D from ?venn.diagram
#' xFig1d = list(
#'   I = c(1:60, 61:105, 106:140, 141:160, 166:175, 176:180, 181:205, 
#'        206:220),
#'   IV = c(531:605, 476:530, 336:375, 376:405, 181:205, 206:220, 166:175, 
#'         176:180),
#'   II = c(61:105, 106:140, 181:205, 206:220, 221:285, 286:335, 336:375, 
#'           376:405),
#'   III = c(406:475, 286:335, 106:140, 141:160, 166:175, 181:205, 336:375, 
#'            476:530)
#'  )
#' get.venn.partitions(xFig1d)
#' grid.draw(VennDiagram::venn.diagram(x, NULL))
#' @export 
get.venn.partitions <- function(x, 
                                force.unique = TRUE, 
                                keep.elements = TRUE, 
                                hierarchical = FALSE){
    #Check typing of arguments
    stopifnot(typeof(x)=="list")
    stopifnot(typeof(force.unique)=="logical")
    
    #Check for empty entries in the list
    emptyInds <- unlist(lapply(x,is.null))
    if(any(emptyInds)){
        warning("removing NULL elements in list.")
        x <- x[!emptyInds]
    }
    
    out <- make.truth.table(x)
    # The assignment of names to x doesn't carry over after the function call. Reassign it from the out dataframe
    names(x) <- names(out) 
    
    # intersect and union will get unique values anyway, but there's no 
    # point in the doing that many times.
    # Behaviour is equivalent to force.unique = TRUE in venn.diagram
    if(force.unique) {
        x <- lapply(x, unique)
    } else {
        x <- lapply(x, function(xRow){
            ret <- data.frame(x=xRow)
            # For aggregating into a count by summing
            ret <- cbind(ret, 1) 
            colnames(ret) <- c("x","n")
            ret <- aggregate(ret,by=list(ret$x),FUN=sum)
            ret$x <- ret$Group.1
            ret$Group.1 <- NULL
            return(ret)
        })
    }
    
    # There are never any values outside all the sets, so don't bother with 
    # case of FALSE in all columns.
    out <- out[apply(out, 1, any), ]
    
    #Compute the descriptive name of the set
    setNames <- apply(out, 1, function(categories){  
        include <- paste(names(x)[categories], collapse = "\u2229") # \u2229 = Unicode intersection
        if(all(categories)) {
            return(include)
        }
        include <- paste0("(",include,")")
        exclude <- paste0("(",paste(names(x)[!categories], collapse = "\u222a"),")") # \u222a = Unicode union
        paste(include, exclude, sep = "\u2216") # \u2216 = Unicode set difference
    })
    
    #Compute the values within the sets
    if(force.unique){
        setValues <- apply(out, 1, function(categories){  
            include <- Reduce(intersect, x[categories])
            exclude <- Reduce(union, x[!categories])
            setdiff(include, exclude)
        })
    } else {
        if(hierarchical){
            setValues <- apply(out, 1, function(categories){  
                #Assume that the number of a certain element is equal to the maximum number of that element in a category.
                #Take the one with the largest in the include group
                #And subtract from it the largest in the exclude group
                
                include <- Reduce(intersect, lapply(x[categories], function(z) z$x))
                intData <- do.call(rbind,x[categories])
                intSum <- aggregate(intData,by=list(intData$x),min)
                #Using the group names appended automatically by aggregate, reassign it to x
                intSum$x <- intSum$Group.1
                intSum$Group.1 <- NULL
                intInds <- intSum$x %in% include
                intSum <- intSum[intInds,]
                
                #If there is nothing to subtract out, then return the result
                if(all(categories)){
                    return(rep.int(intSum$x,intSum$n))
                }
                
                #Find the categories to subtract out
                
                unionData <- do.call(rbind,x[!categories])
                unionSum <- aggregate(unionData,by=list(unionData$x),max)
                #Using the group names appended automatically by aggregate, reassign it to x
                unionSum$x <- unionSum$Group.1
                unionSum$Group.1 <- NULL
                
                #Find the overlapping values
                overlapEle <- intersect(unionSum$x,intSum$x)
                
                #Index into the intersection set and the union set for subtraction
                intSum[match(overlapEle,intSum$x),2] <- pmax(intSum[match(overlapEle,intSum$x),2] - unionSum[match(overlapEle,unionSum$x),2],0)
                
                return(rep.int(intSum$x,intSum$n))
            })
        } else {
            setValues <- apply(out, 1, function(categories){  
                include <- Reduce(intersect, lapply(x[categories], function(z) z$x))
                exclude <- Reduce(union, lapply(x[!categories], function(z) z$x))          
                #The unique names of the values to include
                y <- setdiff(include, exclude)
                
                totalData <- do.call(rbind,x[categories])
                totalSum <- aggregate(totalData,by=list(totalData$x),sum)
                #Using the group names appended automatically by aggregate, reassign it to x
                totalSum$x <- totalSum$Group.1
                totalSum$Group.1 <- NULL
                #Find the x's that are in the actual set
                xInds <- totalSum$x %in% y
                totalSum <- totalSum[xInds,]
                return(rep.int(totalSum$x,totalSum$n))
            })
        }
    }
    
    #Process the list of numbers into strings for easy cbinding
    setEle <- as.matrix(setValues)
    
    #Compute the total number of elements within each set
    setNum <- unlist(lapply(setValues,length))
    
    #Bind all of the output together
    out <- cbind(out,setNames)
    out <- cbind(out,setEle)
    out <- cbind(out,setNum)
    
    colnames(out)[(ncol(out)-2):(ncol(out))] <- c("..set..","..values..","..count..")
    
    #If the actual elements of the sets are not to be printed, remove them
    if(!keep.elements){
        out <- out[,-(ncol(out)-1)]
    }
    
    #Make the output of the set a character vector instead of a factor so you can encode the output
    out$..set.. <- as.character(out$..set..)
    
    Encoding(out$..set..) <- "UTF-8"
    return(out)
}

#' a convinient wrapper that a list as an argument and produces an upset plot, 
#' assuming that each list element is a set. If list is not named random names 
#' will be assigned
#' @param setList a list of sets. If list is unnamed unique names will be created 
#' @param elementsToShow a vector with elements from setList to be shown at the 
#' upset plot
#' @min_size an integer, minimal number of observations in an intersection  to be displayed
#' @param intersectionBarTextSize an integer. The font size in points for 
#' intersection plot bar labels (default 17) 
#' @param matrixTextSize an integer, The font size in points for intersection
#' matrix (default  is 15)
#' @param elementLabelSize an integer. The font size in points (default is 17) 
#' for  element labels stored in elementsToShow 
#' @param angle a numeric. The rotation angle of set intersection size labels 
#' @param intersectTextSize - numeric, font size (in points, default = 20) for 
#' intersect axis labels
#' @param intersectPlot_vjust a numeric, changes position of intersection bar 
#' size labels. This parameter requires adjustment when you change angle 
#' 
#' @examples 
#' \donttest{
#' setList<- list(
#'              name4 = c("Acog", "Birc", "Def6", "Elf3", "Gr"), 
#'             name2 = c("Acog","Gr", "Xist","Vegf", "Ptgs9", "Oct1"),
#'             name3 = c("Acog","Birc","Zpp626","Sam","Yy1","Itgf5","Uudu"),
#'             name1 = c("Itf6","Acog","Nelfb","Wst","Qqe","Elf3","Jmg5","Lef1")) 
#' 
#' elementsToShow <- c("Elf3","Acog","Def6")
#' 
#' upsetPlot = upsetWrapper(setList = setList, elementsToShow = elementsToShow, 
#' min_size = min_size , 
#' intersectionBarTextSize = 17, 
#' intersectTextSize = 20,
#' matrixTextSize = 20,
#' elementLabelSize = 6, 
#' angle = 90,
#' intersectPlot_vjust = 0.5)
#' }
#' @import dplyr
#' @import ComplexUpset     
#' @import tibble  
#' @import magrittr
#'
#' @note uses listToTruthTable() function to build a truth table from input list
#' 
#' @export
upsetWrapper <- function(setList = NULL, min_size = 1, 
                         elementsToShow = NULL, 
                         intersectionBarTextSize = 17, 
                         matrixTextSize = 15,
                         intersectTextSize = 20,
                         elementLabelSize = 17, 
                         angle = 90, 
                         intersectPlot_vjust = 0.5){
  
  if(is.null(names(setList))){warning("setList is not a named list! Unique names will be assigned")
    names(setList) = paste0("Name",1:length(setList))
  }
  #convert 
  df <- listToTruthTable(setList)
  
  #parameters in upset_data() and upset must be the same, othewise the data and
  #plot will not match
  upsetPlotData <- ComplexUpset::upset_data(df, names(df[-1]),
                                            sort_intersections_by = c("degree"),
                                            min_size = min_size)
  
  #a dataframe that stores the size of all intersections 
  intersectionPlot <-  upsetPlotData$sizes$exclusive_intersection %>% rev() %>% 
    data.frame() %>% magrittr::set_colnames(c("count")) %>% 
    rownames_to_column(var = "intersection") %>% 
    rownames_to_column(var = "x") %>% 
    mutate(x = as.numeric(x))
  
  maxCount <- max(intersectionPlot$count) 
  maxSet <-  sapply(setList, length) %>% max()
  #a smaller data frames that select only those intersections that contain elements
  #of interest given by elementsToShow
  sel <- upsetPlotData$with_sizes %>% 
    dplyr::filter(toupper(value) %in% toupper(elementsToShow) & in_exclusive_intersection == 1) %>% 
    dplyr::select(value,intersection)
 
  sel <- left_join(sel,intersectionPlot,by = "intersection")
  
  upsetPlot <- 
    ComplexUpset::upset(  data = df,
                          intersect = names(df[-1]),
                          sort_intersections_by = c("degree"), 
                          min_size = min_size,
                          themes=upset_modify_themes(
                            list(
                              'intersections_matrix' = theme(
                                text = element_text(size = matrixTextSize),
                                axis.title.x = element_blank()),
                              'Intersection size' = theme(text = element_text(
                                size = intersectTextSize)))
                            ),
                          
                          base_annotation = list(
                            'Intersection size'= intersection_size(
                              text_colors = c( on_background='red', 
                                               on_bar='green'),
                              counts = TRUE,
                              text = list(size = intersectionBarTextSize/.pt , #to convert point to mm
                                          vjust = intersectPlot_vjust,
                                          hjust = -0.1, angle = angle)) 
                              + ylim(0, maxCount + maxCount*0.1)
                              + {if(nrow(sel) > 0){    
                                geom_label_repel(data = sel,
                                                 aes(x=x, y=count, label = value, 
                                                     fill = factor(value)),
                                                 min.segment.length = 0,
                                                 nudge_y = maxCount*0.05,
                                                 nudge_x = 0.1,
                                                 force = 20,
                                                 segment.curvature = 0.3,
                                                 segment.ncp = 5,
                                                 segment.color = "gray50",
                                                 segment.square = F,
                                                 segment.linetype = 3,
                                                 size = elementLabelSize/.pt,
                                                 max.overlaps = Inf)}
                              }
                              + theme(legend.position ="none", 
                                    panel.grid = element_blank())
                          ), 
                          set_sizes = (
                            upset_set_size(position = 'right',
                                           geom = geom_bar(fill = "gray70", 
                                                           color = "black"))
                          ) 
                            + geom_text(aes(label = ..count..), hjust = 1.1, 
                                      nudge_y = maxSet*0.15, stat = 'count', 
                                      size = 14/.pt) #to convert to mm
                            #removes axis label and title from set size plot
                            + theme(legend.position ="none", panel.grid = element_blank(),
                                  axis.title.x = element_blank(),
                                  axis.text.x = element_blank()
                                  )
    )
  
  return(list(plot = upsetPlot,intersection_count = intersectionPlot))
  
}

#' This function process gene names from Shiny input forms to produce a formatted
#' vector that can be further used to extract data from wherever 
#' @param geneInput input$xxx variable with gene names from Shiny
geneNameInputProcessing <- function(geneInput){
    genes <- isolate(geneInput) %>%
        toupper() %>%
        strsplit(., split="[, ;]+") %>%
        unlist() %>% 
        unique() 
    return(genes)
}

#' all clustering methods of hclust() are available through clustMethod argument.
#' 
#' REQUIRES 
#' dendextend and stringr
#' 
#' Custom colors could be provided via node.cols
#' 
#' @param dataMatrix a matrix of samples to be clustered. Samples are in columns
#' @param distMethod distance methods from  dist() function. The following methods 
#' are currently supported - euclidean", "maximum", "manhattan", "canberra", "binary".
#' "minkowsky" probably will work but only with the default parameters
#' @param clustMethod clustering methods from hclust function. the following methods 
#' are currently available ward.D", "ward.D2", "single", "complete", "average", 
#' "mcquitty", "median", "centroid"
#' @param samples a vector of sample names. could be columnnames from dataMatrix
#' @param downSample an integer. The number of features (genes, peaks), to be used 
#' for calculating dendrograms. When given the genes are sampled at random, hence, 
#' don't give small numbers to avoid spurious results
#' @param group a vector of group names. should be the same length as sample names
#' @param node.cols a vector of group colors should have the same length as unique(group)
#' if ignored the dafault colors stored by the function will be used. If the number of
#' group is more than 10 randomcolors will be selected. NO guarantees there
#' @param doSeriate see \link[dendextend]{seriate_dendrogram} for details
#' @param expandLimitY a parameter that expand the  axis in ggplot image to accommodate
#' long sample names. The default value -70 was meant for a very long names
#' @param horiz controls tree orientation horiz=FALSE - produces a "vertical" tree
#' with the root node on top and branches pointing down (hanging leaves style).
#' horiz=TRUE produces a sidewise tree with the root note to the left and branches 
#' extending to the right. Useful for a large number of samples  
#' 
#' @return A ggplot object
#' 
#' @examples 
#' sampleDend(dataMatrix = edgeROut$transformedCounts[,-1],
#'  distMethod = "euclidean",  
#'  clustMethod = "complete",
#'  samples = rownames(edgeROut$experimentDetails$experimentVariables),
#'  group = edgeROut$experimentDetails$experimentVariables$group,
#'  node.cols = NULL,
#'  expandLimitY = -70,
#'  horiz = T)
#'  
#'  
sampleDend <- function(dataMatrix,
                       downSample = NULL,
                       randomSample = FALSE,
                       distMethod = "euclidean",  
                       clustMethod = "complete",
                       samples = NULL,
                       group = NULL,
                       node.cols = NULL,
                       doSeriate = TRUE, 
                       doHang = TRUE,
                       labFontSize = 1) {
    if(missing(dataMatrix)){
        stop("dataMatrix is missing with no default")
    }
    #number of samples and number of groups should be the same
    if(length(samples) != length(group)){
        stop("Number of samples must match the number in groups")
    }
    if(length(samples) != ncol(dataMatrix)){
        stop("Number of samples does not match number of columns in dataMatrix")
    }
    distMethod <- match.arg(arg = distMethod, choices = c("euclidean", "maximum", "manhattan", 
                                                          "canberra", "binary", "minkowski"))
    
    clustMethod <- match.arg(arg = clustMethod, choices = c("ward.D", "ward.D2", "single", 
                                                            "complete", "average", "mcquitty", 
                                                            "median", "centroid"))
    ###number of groups - get from $experimentDetails
    groupN <- length(unique(group))
    if(is.null(node.cols)){
        node.cols = rep("black", groupN)
    } else if(length(node.cols) != groupN) {
        stop("Length of node.cols must match the number of sample groups")
    }
    
    if(!is.null(downSample)){
        if(!is.numeric(downSample) || length(downSample) != 1){
            stop("downSample must be numeric of length one or NULL")
        } else if(downSample > nrow(dataMatrix)){
            stop("downSample must be less than the number of rows in dataMatrix")
        } else if(isTRUE(randomSample)){
            dataMatrix <- dataMatrix[sample(1:nrow(dataMatrix), downSample),]
        } else {
            dataMatrix <- dataMatrix[1:downSample,]
        }
    }

    # base dendrogram
    xdist <- t(dataMatrix) %>% scale() %>% dist(., method = distMethod) 
    xdend <- hclust(xdist, method = clustMethod) %>% as.dendrogram() 

    # match samples to groups to nodes
    groupSampleVec <- setNames(group, samples)
    leafLabs <- dendextend::get_leaves_attr(xdend, attribute = "label")
    node.col.val <- as.numeric(unname(groupSampleVec[match(leafLabs, names(groupSampleVec))]))
    # set the colors for each node
    nod.pal <- node.cols[node.col.val]
    #add decorations, convert to ggplot object
    doHang <- ifelse(doHang, 0.1, -0.1)
    xdend <- xdend %>% 
        dendextend::set("leaves_pch", 19) %>%  # leaves point type
        dendextend::set("leaves_cex", 4) %>%  # leaves point size
        dendextend::set("leaves_col", nod.pal) %>% #leaves, colors 
        dendextend::hang.dendrogram(hang = doHang) %>% # hang the leaves
        dendextend::set("labels_cex", labFontSize)

    if(doSeriate){
        xdend <- dendextend::seriate_dendrogram(dend = xdend, x = xdist)
    }
    #Breakpoints for tickmarks based on maximum branch length    
    breaks <- pretty(0:max(dendextend::get_branches_heights(xdend)))
    # how much to offset the labels
    # N.B.: this works well for all but the shortest dednrograms
    offsetLabs <- -1*round(max(breaks)/50)
    p <- ggplot2::ggplot(xdend, 
                         theme = dendextend::theme_dendro(), 
                         offset_labels = offsetLabs, 
                         horiz = FALSE) +
        ggplot2::labs(x = "", y = "") +
        ggplot2::expand_limits(y = -120) + 
        ggplot2::scale_x_continuous(expand =  ggplot2::expansion(mult = c(0, 0.1))) +
        ggplot2::scale_y_continuous(breaks = breaks) +
        ggplot2::annotate(x = 0, xend = 0, y = 0, yend = max(breaks), 
                          colour="black", lwd = 1, geom = "segment") + 
        ggplot2::theme(axis.line.y = ggplot2::element_blank(), 
                               axis.text.y = ggplot2::element_text(size = 15),
                               axis.ticks.y = ggplot2::element_line(),
                       plot.margin = margin(t = 0, r = 10, b = 120, l = 10)) +
        coord_cartesian(clip = 'off')
    return(p)
}


#
#' Better circle scaling...maybe it exists already
#'
#' @param init A value to start incrementing at
#' @param max A maximum value for circle size
#'
#' @noRd
#' @keywords internal
# circle.sizes <- function(init = 5, max = 500){
#     val <- init
#     out <- list()
#     iter <- 1
#     while(val < max){
#         val <- 2*val
#         if(val >= max){
#             out[iter] <- max
#         } else {
#             out[iter] <- val
#         }
#         iter <- iter+1
#     }
#     return(unlist(out))
# }

#' Function for modifying the legend of plotlyified ggplots
#'
#' @param pp A plotly object
#'
#' @noRd
#' @keywords internal
modifyLegend <- function(pp){
    l <- length(pp$x$data)
    n <- sapply(pp$x$data, \(x) x$name)
    str_matrix <- stringr::str_split(n, ",|\\(|\\)", simplify = T)
    # v1 is the color
    v1 <- str_matrix[, max(1, ncol(str_matrix)-2)]
    # v2 is shape
    v2 <- str_matrix[, max(1, ncol(str_matrix)-1)]
    # NB> modify pcaPlot to put aes for color and shape in geom_point in that order
    v1lu <- length(unique(v1))
    v2lu <- length(unique(v2))
    nLeg <- v1lu + v2lu
    c1 <- sapply(pp$x$data, \(x) x$marker$color) |> unique()
    c2 <- sapply(pp$x$data, \(x) x$marker$symbol) |> unique()
    # make fake data for legend
    pp$x$data[(l+1):(l+nLeg)] <- replicate(nLeg, pp$x$data[[1]], simplify = F)
    # remove the default legend
    for(i in 1:l){
        pp$x$data[[i]]$showlegend <- F
    }
    # set the coordinates of the fake data to NA
    for(i in (l+1):(l+nLeg)){
        pp$x$data[[i]]$x <- NA
        pp$x$data[[i]]$y <- NA
    }
    # modify the first n elements to be color with color labels
    if(v2lu > 1){
        for(i in 1:v1lu){
            pp$x$data[[i+l]]$marker$symbol <- "square"
            pp$x$data[[i+l]]$marker$color <- c1[i]
            pp$x$data[[i+l]]$marker$line$color <- c1[i]
            pp$x$data[[i+l]]$legendgroup <- "1"
            pp$x$data[[i+l]]$name <- unique(v1)[i]
        }
    } else { # if there is no shape variable just leave the symbol as is...
        for(i in 1:v1lu){
            pp$x$data[[i+l]]$marker$color <- c1[i]
            pp$x$data[[i+l]]$marker$line$color <- c1[i]
            pp$x$data[[i+l]]$legendgroup <- "1"
            pp$x$data[[i+l]]$name <- unique(v1)[i]
        }
    }
    # modify the next m elements to be unique shapes with shape labels
    if(v2lu > 1){
        for(i in 1:v2lu){
            pp$x$data[[i+l+v1lu]]$marker$symbol <- c2[i]
            pp$x$data[[i+l+v1lu]]$marker$color <- "rgba(0,0,0,1)"
            pp$x$data[[i+l+v1lu]]$marker$line$color <- "rgba(0,0,0,1)"
            pp$x$data[[i+l+v1lu]]$legendgroup <- "2"
            pp$x$data[[i+l+v1lu]]$name <- unique(v2)[i]
        }
    } else {
        pp$x$data[[1+l+v1lu]]$showlegend <- F
    }
    pp$x$layout$legend$title$text <- ""
    return(pp |> layout(legend = list(x = 100, y = 0.5)))
}


#' Utility for rounding to specific values
#' 
#' @param x a numeric value to be rounded
#' @param n a number to which x should be rounded
#' 
#' @examples
#' 
#' # rounds to nearest supplied value
#' round2n(-0.5569, 0.001) # [1] -0.557
#' round2n(-0.5569, 0.01) # [1] -0.56
#' round2n(-0.5569, 0.1) # [1] -0.6
#' 
#' # similarly works for values greater than 1
#' round2n(5569, 2) # [1] 5568
#' round2n(5569, 10) # [1] 5570
#' round2n(5569, 500) # [1] 5500
#' round2n(5569, 1000) # [1] 6000
#' 
#' @noRd
#' @keywords internal
#' @export
round2n <- function(x, n){
    round(x/n)*n
}

#' replot a vizNetwork graph as ggraph
#' 
#' @param nodeCoords A dataframe of node positions generated by visGetPositions
#' @param networkData A list of node and edge information generated by buildTFNet
#' 
#' @export
replotVizNetworkGraph <- function(nodeCoords, networkData){
    # rename contrast column to just stat name
    oldColCol <- networkData$nodeInfo |> select(ends_with(c("logFC", "PValue", "FDR"))) |> colnames()
    newColCol <- stringi::stri_split(str = oldColCol, regex = "\\.")[[1]][2]
    networkData$nodeInfo <- rename(networkData$nodeInfo, !!sym(newColCol) := !!sym(oldColCol))
    # if the data are not logFC, rescale to -log10 as usual for p-value
    if(newColCol != "logFC"){
        networkData$nodeInfo[[newColCol]] <- -log10(networkData$nodeInfo[[newColCol]])
    } 
    # get rank of color vector and extract exact colors used by visNetwork
    networkData$nodeInfo$order <- sort.list(networkData$nodeInfo[[newColCol]], method = "radix")
    colVecOrder <- networkData$nodeInfo$color[networkData$nodeInfo$order]
    midVal <- round(length(colVecOrder)/2)
    # the colors to use
    colMin <- colVecOrder[1]
    # mid color is always white for logFC
    colMid <- ifelse(newColCol != "logFC", colVecOrder[midVal], "#f7f7f7")
    colMax <- colVecOrder[length(colVecOrder)]
    # values for color gradient
    colMinVal <- round2n(min(networkData$nodeInfo[[newColCol]]), 0.1)
    # midpoint is always 0 for logFC
    colMidpoint <- ifelse(newColCol != "logFC", networkData$nodeInfo[[newColCol]][networkData$nodeInfo$order][midVal], 0)
    colMaxVal <- round2n(max(networkData$nodeInfo[[newColCol]]), 0.1)
    # color limits
    if(newColCol != "logFC"){
        # in log10 space the lowest value is 0 and the largest can be 15 (when p ~ 0)
        # set the lower limit to slightly below 0 to make sure 0 is included in legend
        lowLim <- -0.05
        # set upper limit to the rounded off maximum value
        uppLim <- ceiling(colMaxVal)
        scaleName <- paste0("-log10(", newColCol, ")")
    } else {
        # since there is no translation and FC is already log space, just add a 
        # little buffer to make sure upper and lower limits are included in legend
        lowLim <- colMinVal-0.05
        uppLim <- colMaxVal+0.05
        scaleName <- "LogFC"
    }
    
    # build the tidygraph object
    tdg <- tbl_graph(nodes = networkData$nodeInfo, edges = networkData$edgeInfo, node_key = "id") |>
        activate(nodes) |>
        left_join(nodeCoords, by = join_by(id)) |>
        mutate(
            shp = case_when(group == "Target" ~ 21L,
                            group == "TF" ~ 24L,
                            group == "TFT" ~ 23L)
        )
    # plot the graph
    ng <- ggraph(graph = tdg, x = x, y = y*-1) +
        geom_edge_fan(strength = 0.25, color = "#606060", edge_width = 0.25,
                      arrow = arrow(angle = 20, length = unit(2.5, 'mm'), type = "closed"),
                      end_cap = circle(3, 'mm')) +
        geom_edge_loop(aes(strength = 150, direction = 45), 
                       end_cap = circle(3, 'mm'), start_cap = circle(3, 'mm'),
                       color = "#606060", edge_width = 0.25, 
                       arrow = arrow(angle = 20, length = unit(2.5, 'mm'), type = "closed")) + 
        geom_node_point(aes(shape = group, fill = !!sym(newColCol)), size = 7, stroke = 0.125) +
        # nudge would need to be scaled to the dimensions of the plot
        geom_node_text(aes(label = label), repel = T) + # , nudge_y = -10 
        scale_fill_gradient2(name = scaleName, low = colMin, mid = colMid, high = colMax, 
                             midpoint = colMidpoint, limits = c(lowLim, uppLim),
                             breaks = extended(dmin = colMinVal, dmax = colMaxVal, m = 5),
                             labels = extended(dmin = colMinVal, dmax = colMaxVal, m = 5)) +
        scale_shape_manual(values = c(21, 24, 23), breaks = c("Target", "TF", "TFT")) +
        theme_void(base_family="sans") +
        theme(plot.margin = unit(c(5.5, 5.5, 5.5, 5.5), "points"))
    
    return(ng)
}

#' A diverging color palette a la colorRampPalette but with control over the central value
#' 
#' For many visualizations it is useful to preserve a central value for diverging
#' palettes in order to avoid confusion about the true value given an expected mid-point
#' value being the central color in a divergent palette.
#' 
#' @param data A matrix, vector, or data.frame for which `quantile` is a valid operation
#' @param brewer.div.pal A character string indicating the color brewer palette to use.
#' It must be one of the divergent palettes. 
#' Default: "RdBu"
#' @param n.pal.colors An integer between 3 and 11 specifying the number of colors to start with. 
#' Default: 11
#' @param reverse.pal A logical indicating whether the palette should be reversed 
#' (i.e. for palette "RdBu", blue is low and red is high)
#' Default: FALSE
#' @param center.on A numeric value indicating what value of the data should be
#' considered the center of the diverging palette
#' Default: 0
#' @param n.vals An integer specifying how many colors to generate. Maximum 256
#' Default: 256
#' @param winsor A numeric vector of length 2 indicating the lowest/highest quantile
#' to be included when generating the color bins. N.B. this parameter must be set 
#' to c(0, 1) when `output = "classInt"`
#' Default: c(0, 1)
#' @param output A character string indicating the style of output, one of 
#' c("list", "vector", "classInt"). `output = "classInt"` is especially useful 
#' for visualizing the palette but requires the `classInt` package. 
#' Default: "list"
#' 
#' @export
divergingColorRampPal <- function(data, 
                                  brewer.div.pal = "RdBu", 
                                  n.pal.colors = 11,
                                  reverse.pal = FALSE,
                                  center.on = 0, 
                                  n.vals = 256, 
                                  winsor = c(0, 1), 
                                  output = c("list", "vector", "classInt")){
    if(n.pal.colors > 11 | n.pal.colors < 3) {
        stop("'RColorBrewer' diverging color palettes must have at least 3 and no more than 11 colors")
    }
    if(n.vals > 256){
        stop("Cannot generate a palette of more than 256 colors")
    }
    if(output == "classInt" & any(winsor != c(0,1))){
        stop("'classInt' output cannot be set when 'winsor' is not c(0,1)")
    }
    output <- match.arg(output, choices = c("list", "vector", "classInt"))
    validPalette <- RColorBrewer::brewer.pal.info |> filter(category == "div") |> rownames()
    brewer.div.pal <- match.arg(brewer.div.pal, choices = validPalette, several.ok = F)
    true.n.cols <- n.pal.colors - 1
    if(n.vals %% true.n.cols != 0){
        warning("'n.pal.colors' cannot be evenly split along 'n.vals', returning closest approximate n.vals: ", 
                floor(n.vals/true.n.cols)*true.n.cols, call. = F, immediate. = T)
        n.vals <- floor(n.vals/true.n.cols)*true.n.cols
    }

    n.half = floor(n.vals/2)
    step.size <- n.vals/true.n.cols
    data.range <- quantile(data, probs = winsor, na.rm = TRUE)
    if(center.on < data.range[1] | center.on > data.range[2]){
        stop("'center.on' must be within the range of the data: ", 
             round(data.range[1], 3), " ... ", 
             round(data.range[2], 3))
    }

    pal <- brewer.pal(n = n.pal.colors, brewer.div.pal)
    if(isTRUE(reverse.pal)){
        pal <- rev(pal)
    }
    rc1 <- lapply(2:n.pal.colors, \(i){
        colorRampPalette(colors = c(pal[i-1], pal[i]), space = "Lab")(step.size)
    }) |> unlist() 

    rb1 <- seq(data.range[1], center.on, length.out = n.half)
    rb2 <- seq(center.on, data.range[2], length.out = n.half+1)[-1]
    rampbreaks <- c(rb1, rb2)
    if(isa(data, "data.frame")){
        data <- unlist(data, use.names = F)
    } else if(isa(data, "array")){
        data <- as.vector(data)
    }
    if(output == "list"){
        res <- list(breaks = rampbreaks, colors = rc1)
    } else if(output == "vector"){
        res <- setNames(rampbreaks, rc1)
    } else if(output == "classInt"){
        cuts <- classInt::classIntervals(data, style = "fixed", fixedBreaks = rampbreaks)
        res <- list(CI = cuts, PAL = rc1)
    }
    return(res)
}
