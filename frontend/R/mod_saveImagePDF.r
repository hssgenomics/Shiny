#----------------Saving an .pdf file of selected values-------------------

#' saveImagePDF UI Function
#' 
#' @description A shiny Module.
#'
#' @param id,input,output,session Internal parameters for {shiny}.
#' @param label character string to be printed on download button
#' @param top if true, places the download button on the top of the plot, otherwise at the bottom
#' this is convenient for a very "tall" plots. You don't have to scroll to look for a download button
#' 
#' @noRd 
#'
#' @importFrom shiny NS tagList 
# savePlotlyPDFUI
mod_saveImagePDF_ui <- function(id, label = "Download PDF File", top = FALSE){
    ns <- NS(id)
    # print(ns("plotUI"))
    if (top == FALSE) {
        tagList(
            uiOutput(outputId = ns("plotUI")),
            p(""),
            div(aligh="center",
                actionButton(inputId = ns("modalCall"), label = label,
                             icon = icon("download"))
            ),
            p("")
        )
    } else {
        tagList(
            div(aligh="center",
                actionButton(inputId = ns("modalCall"), label = label,
                             icon = icon("download"))
            ),
            p(""),
            uiOutput(outputId = ns("plotUI")),
            p("")
        )
    }
}

#savePlotlyPDF 
#' @param id Internal parameters for {shiny}.
#' @plotlyToSave plot object to be saved
#' @prefix a string to be added to the file name
#' @objectType a type of object to be plotted
#' @renderHeight this is for rendering
#' @chromote
#' @delay
#' @return 
#' @height this is for saving
#' @width  this is for saving
#' 
mod_saveImagePDF_server <- function(id, plotlyToSave, prefix = "", objectType = "plotly",
                                    renderHeight = 400, chromote = FALSE, delay = 2, 
                                    units = "in", height = 7, width = 9){ 
    
    moduleServer(id, function(input, output, session){
    
        
        displayInfo <- session$userData$displayInfo
        
        
        
        
        ns <- session$ns
        # print(ns("plotToBeSaved"))
        #create a UI to render the reactive variable passed to the module 
        #it has to be done in a server because we do not know whether an
        #incoming object is plotly or ggplot - they require different *Output and 
        #render* functions   
        
        #observe(print(class(plotlyToSave())))
        
        output$plotUI <- renderUI({
            if (objectType == "plotly") {
                plotlyOutput(ns("plotToBeSaved"), height = renderHeight) %>%
                    withSpinner(color = "#0088cf", size = 1.5)
            } else {
                plotOutput(ns("plotToBeSaved"), height = renderHeight) %>%
                    withSpinner(color = "#0088cf", size = 1.5)
            }
        })
        #render actual plot, again depending on the type of an incoming object                
        if(objectType == "plotly") {
            scatterCheck <- reactive({
                hasScatter <- any(sapply(plotlyToSave()$x$attrs, "[[", "type") == "scatter")
                if(hasScatter){
                    hasHeatmap <- any(sapply(plotlyToSave()$x$attrs, "[[", "type") == "heatmap")
                    if(hasHeatmap){
                        return(FALSE)
                    } else {
                        return(TRUE)
                    }
                } else {
                    return(FALSE)
                }
            })
            output$plotToBeSaved <- renderPlotly({ 
                if(scatterCheck()){
                    plotlyToSave() %>% config( displaylogo = FALSE) %>% toWebGL() 
                } else {
                    plotlyToSave() %>% config( displaylogo = FALSE)
                }
            })
        } else {
            output$plotToBeSaved <- renderPlot({ plotlyToSave() })
        }
        
        namepdf = paste0('RNAviz_Plot_', prefix, Sys.Date(), ".pdf") 
        namehtml = paste0('RNAviz_Plot_', prefix, Sys.Date(), ".html")
        
        #this is a download flag that changes in downloadHandler and used to close modal
        # windows when downloadButton is pressed
        isDownloaded <- reactiveValues (setDownload = 0)
        
     
        #consider replacing modalDialog() by shinyjqui::draggableModalDialog()
        observeEvent(input$modalCall,{
            
            showModal(     
                modalDialog(
                    splitLayout(
                        numericInput(inputId = ns("width"),
                                     label = "PDF Width:",
                                     value = ifelse(class(width)[1] == "reactiveExpr",round(width(),2),round(width, 2)),
                                     min = 1,  step = 1),
                        numericInput(inputId = ns("height")  ,
                                     label = "PDF Height:",
                                     value = ifelse(class(height)[1] == "reactiveExpr",round(height(),2),round(height,2)),
                                     min = 1,  step = 1),
                        selectInput(inputId = ns("units"),
                                    label = "PDF size units:",
                                    choices = c("in"),
                                    selected = units)
                    ), 
                    
                    span("We attempt to provide sensible default figure sizes, ",
                         "however, we are not perfect.",
                         "This is where you can change your figure sizes, or 
                             just press Accept"),
                    
                    footer = tagList(
                        modalButton(label = "Cancel"),
                        downloadButton(outputId = ns("accept"), 
                                       label = "Accept"))
                    
                )
            )
        })
        
        output$accept <- downloadHandler(
            filename = namepdf,
            content = function(file, 
                               width = input$width, 
                               height = input$height, 
                               units = input$units){
                 
                #correct
                # observe({print(width)
                 #          print(height)
                 #          print(units)
                 #     })
                withProgress(message = 'Saving PDF', 
                             style = "notification", 
                             value = 0, {
                                 #I hate it, but there is nothing better so far
                                 for (i in 2:6) {
                                     incProgress(1-1/(i-1))
                                     Sys.sleep(0.01)
                                 }
                                 
                                 if(is(plotlyToSave(), "plotly")){
                                     filename = namehtml 
                                     
                                     # we do not use webshot anymore eventually the commented
                                     #section below should be deleted 
                                     # switch( units,
                                     #         px = { vwidth_px = width; 
                                     #                vheight_px = height},
                                     #         
                                     #         `in` = { vwidth_px = width*displayInfo$screenDPI; 
                                     #                  vheight_px = height*displayInfo$screenDPI
                                     #                    },
                                     #         
                                     #          cm = {vwidth_px = 0.393*height*displayInfo$screenDPI;
                                     #               vheight_px = 0.393*height*displayInfo$screenDPI}
                                     #         )
                                     
                                     
                                     #We use chromote to generate qusage bouble plots. 
                                     if (!chromote) {
                                         
                                         # this is a placeholder in case we 
                                         #need a different pdf rendering mechanism
                                     } else {
                                         #TODO we need to revisit pixels/cm at some point
                                         if(units == "px"){
                                             units = "in" 
                                             width = width*displayInfo$screenDPI
                                             height = height*displayInfo$screenDPI
                                             }
                                         
                                         
                                         tmp.files <- tempfile()
                                         namehtml <- paste0(tmp.files, ".html")
                                         on.exit(unlink(namehtml), add = TRUE) 
                                         on.exit(unlink(tmp.files, recursive = TRUE), add = TRUE)
                                         
                                         
                                         htmlwidgets::saveWidget(plotlyToSave() %>% config(displayModeBar = F) , 
                                                    file = namehtml)
                                         
                                   
                                         b <- ChromoteSession$new(width = ceiling(width*displayInfo$screenDPI), 
                                                                  height = ceiling(height*displayInfo$screenDPI)
                                                                  )
                                         
                                         b$Page$navigate(paste0("file://", namehtml))
                                         b$Page$loadEventFired()
                                         
                                         b$screenshot_pdf(namepdf, pagesize =c(width, height),
                                                          units = units, margins = 0)
                                         invisible(b$close())
                                     }
                                     
                                     file.copy(namepdf, file, overwrite = TRUE)
                                     shiny::incProgress()
                                     file.remove(namehtml)
                                     isDownloaded$setDownload = 1 #marking end of download
                                 } else if("gg" %in% class(plotlyToSave()) ){
                                     ggsave(namepdf, plotlyToSave(), device = "pdf", units = input$units, 
                                            height = input$height, width = input$width)
                                     file.copy(namepdf, file, overwrite = TRUE)
                                     isDownloaded$setDownload = 1
                                 }
                             })
                if(isDownloaded$setDownload > 0)removeModal()
            }
        )
    })
}
