#----------------Saving an .cvs file of selected values-------------------
#dataToSave - takes a reactive dataframe in a form (reactive(your_variable))
mod_saveSelectedData_ui <- function(id, label = "Download Data, .CSV"){
    ns <- NS(id)
    tagList( 
        downloadButton(outputId = ns("download"), label = label)
    )
}

mod_saveSelectedData_server <- function(id,   dataToSave, prefix = '_'){
    
    moduleServer(id, function(input, output, session){    
        
        ns <- session$ns
        
        filename = paste0('Data_selection_', prefix, Sys.Date(), '.csv')
    
        output$download <- downloadHandler(
            filename = function(){ filename },
            content =  function(file){
                if (!is.empty(dataToSave())) {
                    write.csv(dataToSave(), row.names = F, file)
                } else { print("Nothing was selected!") }  
            })
 })
}