#----------------Clipboard-----------------
#current idea - take an object flatten/coerse it to a vector and keep it until paste is requested 

mod_pseudoClip_ui <- function(id, label = "View selected gene names"){
    ns <- NS(id)
    tagList(
        actionButton(inputId = ns("view"), label = label)
    )
}

mod_pseudoClip_server <- function(id, varCopied){
    
    moduleServer(id, function(input, output, session){
        
        ns <- session$ns
                  
        observeEvent(input$view, ignoreInit = TRUE, {
            showModal(
                modalDialog(
                    title = "Highlight and use CTRL-C to copy gene names to clipboard",
                    HTML(varCopied()),
                    easyClose = TRUE, 
                    footer = tagList(modalButton(label="Dismiss"))
                )
            )
    })
 })        
}