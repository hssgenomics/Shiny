
#rsconnect need to know about  Bioconductor
library(BiocManager)
options(repos = BiocManager::repositories())

#call rsconnect
library(rsconnect)

#tocken and is secret should be obtain 
rsconnect::setAccountInfo(name='hssgenomics',
                          token='',
                          secret='')

rsconnect::deployApp('/home/yurii/R_projects/Shiny/frontend/',appName="RNAseq_DRaMA")
 