var previousRow = null; //Store previous row 


function scrollToSelectedRow(args) {
  const tableId = args.tableId;
  const selectedRow = args.selectedRow;    
  const color = args.color || 'white';
 
  
  const table = $('#' + tableId + ' table').DataTable();
  const rowNode = table.row(selectedRow - 1).node();
  
  
  // Reset the color of the previously selected row
  if (previousRow !== null) {
    $(previousRow).css('background-color', '');
  }
  
  rowNode.scrollIntoView(false);
  
// add color as an argument I think js calls them parametrs, 

  $(rowNode).css('background-color', color);
// make now Row  previous  
  previousRow = rowNode;
  
}

// Register the Shiny custom message handler
Shiny.addCustomMessageHandler('scrollToSelectedRow', scrollToSelectedRow);


