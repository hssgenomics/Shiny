// This script is waiting for the Shiny connection and then sets up event listeners
$(document).on('shiny:connected', function() {
  // Custom handler function to attach Plotly click event listener
  function PlotlyCtrlClick(el) {
    el.on('plotly_click', function(e) {
      if(e.event.ctrlKey) {
        var point = e.points[0];
        Shiny.setInputValue('clickedPoint', {
          x: point.x,
          y: point.y,
          Pathway: point.customdata
        }, {priority: 'event'});
      }
    });
  }
  
  // Export the function to be used in onRender. This part I don't fully understand
  // it seems we want this function to be used in a specific context of when called by
  //onRender from inside plotlyOuput, but it needs to be available globally so we attach it
  // to window object which represent browser windiw that runs the script
  window.PlotlyCtrlClick = PlotlyCtrlClick;
});