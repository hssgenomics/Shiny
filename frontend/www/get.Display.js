$(document).ready(function() { //(document).ready runs the function only after the HTML was loaded
  //console.log('Document ready');

  function getScreenInfo() {
    var screenRes = {
      width: window.screen.width,
      height: window.screen.height,
      //dpi is really not dpi :) but I am trying to closely approximate it
      dpi: window.devicePixelRatio*96 
    };

    //console.log('Screen Parameters:', screenRes); good for debugging
    // I hope I am doing it right
    // Checks if Shiny and Shiny.setInputValue are defined and if Shiny.setInputValue is a function.
    // if yes 
    if (typeof Shiny !== 'undefined' && typeof Shiny.setInputValue === 'function') {
      Shiny.setInputValue('screenRes', screenRes);
    } else {
      console.error('Shiny.setInputValue is not available.');
    }
  }
// see here https://shiny.posit.co/r/articles/build/js-events/
// shiny:connected is an event that gets tregerred when shiny is connected and, hopefully
// internal shiny functions become available. at this point we call getScreenInfo()
// which creates screenRes object and makes it available via Shiny.setInputValue
//as input$screenRes
  $(document).on('shiny:connected', function() {
    getScreenInfo();
  });
});